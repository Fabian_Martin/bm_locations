<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Bitmotion.BmLocations',
    'Map',
    'Locations Map'
);
$pluginSignature = str_replace('_', '', $_EXTKEY) . '_map';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/Flexforms/locations_map.xml');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Bitmotion.BmLocations',
    'List',
    'Locations List'
);
$pluginSignature = str_replace('_', '', $_EXTKEY) . '_list';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/Flexforms/locations_list.xml');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Bitmotion.BmLocations',
    'Pdfexport',
    'Locations PDF-Export'
);
$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pdfexport';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/Flexforms/locations_pdf.xml');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'bm_locations');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_division', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_division.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_division');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_competence', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_competence.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_competence');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_region', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_region.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_region');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_location', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_location.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_location');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_consultant', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_consultant.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_consultant');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_region_map', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_region_map.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_region_map');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_zip_range', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_zip_range.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_zip_range');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_consultanttype', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_consultanttype.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_consultanttype');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bmlocations_domain_model_contact', 'EXT:bm_locations/Resources/Private/Language/locallang_csh_tx_bmlocations_domain_model_contact.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bmlocations_domain_model_contact');
