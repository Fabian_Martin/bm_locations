<?php
$classMap = array();

    $extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('bm_locations');
    $classMap = array(
        'Bitmotion\BmLocations\Utility\Geo\TCEforms' => $extensionPath . 'Classes/Utility/Geo/TCEforms.php',
    );

return $classMap;