<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
	{
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1461871364] = array(
            'nodeName' => 'map',
            'priority' => '50',
            'class' => 'Bitmotion\BmLocations\Utility\Geo\GoogleMap',
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Bitmotion.BmLocations',
            'Map',
            [
                'Map' => 'list, ajax',
                'Search' => 'list'
            ],
            // non-cacheable actions
            [
                'Map' => '',
                'Search' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Bitmotion.BmLocations',
            'List',
            [
                'Competence' => 'list, show',
                'Location' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Competence' => '',
                'Location' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Bitmotion.BmLocations',
            'Pdfexport',
            [
                'Competence' => 'list, show',
                'Location' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Competence' => '',
                'Location' => ''
            ]
        );

	// wizards
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
		'mod {
			wizards.newContentElement.wizardItems.plugins {
				elements {
					map {
						icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/user_plugin_map.svg
						title = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_map
						description = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_map.description
						tt_content_defValues {
							CType = list
							list_type = bmlocations_map
						}
					}
					list {
						icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/user_plugin_list.svg
						title = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_list
						description = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_list.description
						tt_content_defValues {
							CType = list
							list_type = bmlocations_list
						}
					}
					pdfexport {
						icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/user_plugin_pdfexport.svg
						title = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_pdfexport
						description = LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bm_locations_domain_model_pdfexport.description
						tt_content_defValues {
							CType = list
							list_type = bmlocations_pdfexport
						}
					}
				}
				show = *
			}
	   }'
	);
    },
    $_EXTKEY
);
