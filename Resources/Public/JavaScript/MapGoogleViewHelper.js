
		L.Circle = L.Circle.extend({
			projectLatlngs: function () {
				var lngRadius = this._getLngRadius(),
						latlng2 = new L.LatLng(this._latlng.lat, this._latlng.lng - lngRadius),
						point2 = this._map.latLngToLayerPoint(latlng2);

				this._point = this._map.latLngToLayerPoint(this._latlng);

				var zoom = this._map.getZoom();

				this._radius = 0.5 + (0.65 * zoom);
			}
		});


		L.CircleMarker = L.CircleMarker.extend({
			options: {
				radius: 10,
				weight: 0,
				className: 'leaflet-region',
				text: false,
				textXOffset: 0,
				textYOffset: 6
			},
			_initPath: function () {
				this._container = this._createElement('g');

				this._path = this._createElement('path');
				this._text = this._createElement('text');

				if (this.options.className) {
					L.DomUtil.addClass(this._path, this.options.className);
					L.DomUtil.addClass(this._text, this.options.className);
				}

				this._container.appendChild(this._path);
				this._container.appendChild(this._text);

				this._text.textContent=this.options.text;
			},

			_updatePath: function () {
				var str = this.getPathString();
				if (!str) {
					// fix webkit empty string parsing bug
					str = 'M0 0';
				}
				this._path.setAttribute('d', str);

				this._text.setAttribute('text-anchor', 'middle');
				this._text.setAttribute('x', this._point.x + this.options.textXOffset);
				this._text.setAttribute('y', this._point.y + this.options.textYOffset);
			}
		});


		function LocationMap(options) {

			this.options = options;
			// State handling ---------

			this.polygonHasFocus = false;
			this.polygonHasFocusZoomLevel = false;

			// callbacks: this.showCountryList, this.clearCountryList, this.setCountrySelectbox;


			// Root vector map ---------------

			this.featureStyles = {};

			this.featureStyles.base = {
				countryStyle: {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '#cccccc'
				},
				countryNonactiveStyle: {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '#f0f0f0',
					clickable: false
				},
				countryHoverStyle: {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '#aaaaaa'
				},
				countrySelectStyle: {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '#aaaaaa'
				}
			};

			// Vector overlay ---------------

			this.featureStyles.hover = {
	 			countryStyle: {
 					color: '#ffffff',
 					weight: 1,
 					opacity: 0,
 					fillOpacity: 0.0,
					fillColor: '#cccccc'
	 			},
	 			countryNonactiveStyle: {
 					color: '#ffffff',
 					weight: 1,
 					opacity: 0,
 					fillOpacity: 0.0,
 					fillColor: '#f0f0f0',
 					clickable: false
	 			},
	 			countryHoverStyle: {
 					color: '#ffffff',
 					weight: 1,
 					opacity: 0,
 					fillOpacity: 0.3,
 					fillColor: '#aaaaaa'
	 			},
	 			countrySelectStyle: {
 					color: '#aaaaaa',
 					weight: 15,
 					opacity: 0.05,
 					fillOpacity: 0.15,
 					fillColor: '#aaaaaa'
	 			},
				regionStyle: {
					color: '#ea6f18',
					weight: 2,
					opacity: 1,
					fillOpacity: 0.2,
					fillColor: '#cccccc'
				},
				regionHoverStyle: {
					color: '#ea6f18',
					weight: 1,
					opacity: 1,
					fillOpacity: 0.3,
					fillColor: '#cccccc'
	 			},
				regionSelectStyle: {
					color: '#ea6f18',
					weight: 2,
					opacity: 1,
					fillOpacity: 0.2,
					fillColor: '#ea6f18'
	 			}
			};


			this.featureStyles.current = this.featureStyles.base;

			// Country Layer: this.countriesBounds, this.franceBounds, this.countryLayer;

			// Tile Layer ---------------

			this.tileLayer;

			// region maps ---------------

			this.regionMap;
			this.markerRegion;
			this.regionMaps = [];


			// Marker: this.markerLayer;


			// Methods ---------------


			this.isActiveLayer = function(layer) {
				if (this.polygonHasFocus && (layer._leaflet_id==this.polygonHasFocus._leaflet_id)) {
					return true;
				}
				return false;
			};


			// Country Methods ---------------


			/**
			 * trigger a 'click' by iso code
			 */
			this.clickCountryByID = function(isoA2)
			{
				var layers = this.countryLayer.getLayers();
				for (var i in layers) {
					var layer = layers[i];
					if (layer.feature.properties.iso_a2 == isoA2) {
						if (!this.isActiveLayer(layer)) {
							layer.fire('click');
						}
						return;
					}
				}
				// it can happen that a valid country is not on the map - we still want to try list
				if (this.polygonHasFocus) {
					this.deselectActiveFeature();
					this.map.fitBounds(this.maxBounds);
				}

				this.updateCountryList(isoA2, this.options.clickOnCountryListContacts);
			};


			/**
			 * select a country on the map by iso code
			 */
			this.selectCountryByID = function(isoA2)
			{
				var layers = this.countryLayer.getLayers();
				var layer;

				for (var i in layers) {
					if (layers[i].feature.properties.iso_a2 == isoA2) {
						if (this.isActiveLayer(layers[i])) {
							return;
						}
						layer = layers[i];
					}
				}

				this.deselectActiveFeature();

				if (layer) {
					this.selectFeatureLayer(layer);
				} else {
					this.map.fitBounds(this.maxBounds);
				}
			};


			/**
			 * deselect the currently selected country
			 */
			this.deselectActiveFeature = function()
			{
				if (this.polygonHasFocus) {
					this.resetHighlightFeatureLayer(this.polygonHasFocus);
					this.polygonHasFocus = null;
					this.hideRegionMap();
					this.info.update();
					if (this.setCountrySelectbox) this.setCountrySelectbox('');
				}
			}

			/**
			 * select a country on the map by layer
			 */
			this.selectFeatureLayer = function(layer)
			{
				var isoA2 = layer.feature.properties.iso_a2;

				this.deselectActiveFeature();

				if (isoA2 == 'FR') {
					var bounds = this.franceBounds;
				} else {
					var bounds = layer.getBounds();
				}
				this.polygonHasFocus = layer;
				this.map.fitBounds(bounds);
				this.polygonHasFocusZoomLevel = this.map.getBoundsZoom(bounds);
				this.info.update(layer.feature.properties);

				if (this.setCountrySelectbox) this.setCountrySelectbox(isoA2);

				if (this.options.regionsFeatures && this.options.regionsFeatures[isoA2]) {
					//this.countryLayer.resetStyle(layer);
					if (layer.feature.properties.is_active) {
						layer.setStyle(this.featureStyles.current.countryStyle);
					} else {
						layer.setStyle(this.featureStyles.current.countryNonactiveStyle);
					};
					this.showRegionMap(isoA2)
					this.markerLayer.bringToFront();

				} else {
					layer.setStyle(this.featureStyles.current.countrySelectStyle);
				}
			}


			/**
			 * reset a layer style
			 */
			this.resetHighlightFeatureLayer = function(layer)
			{
				if (layer.feature.properties.is_active) {
					layer.setStyle(this.featureStyles.current.countryStyle);
				} else {
					layer.setStyle(this.featureStyles.current.countryNonactiveStyle);
				}
			};


			/**
			 * Event handler
			 * This is what happens when your mouse hovers over a country map element.
			 */
			this.highlightFeatureEvent = function(e)
			{
				var layer = e.target;

				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					layer.setStyle(this.featureStyles.current.countryHoverStyle);
					this.info.update(layer.feature.properties);
				}
			};


			/**
			 * Event handler
			 * This is what happens when your mouse goes away from an country element.
			 */
			this.resetHighlightFeatureEvent = function(e)
			{
				var layer = e.target;
				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					this.resetHighlightFeatureLayer(layer);
				}

				if (this.polygonHasFocus) {
					this.info.update(this.polygonHasFocus.feature.properties);
				} else {
					this.info.update();
				}
			}


			/**
			 * Event handler
			 * Handles clicks on a country
			 */
			this.clickFeatureEvent = function(e)
			{
				var layer = e.target;
				var isoA2 = layer.feature.properties.iso_a2;

				this.featureStyles.current = this.featureStyles.hover;

				var clickOnCountryListContacts = this.options.clickOnCountryListContacts;
				if (this.options.regionsFeatures && this.options.regionsFeatures[isoA2]) {
					clickOnCountryListContacts = false;
				}

				if (this.options.countryCount == 1)  {

					this.selectFeatureLayer (layer);
					this.updateCountryList(isoA2, clickOnCountryListContacts);

				} else if (this.isActiveLayer(layer) && (this.polygonHasFocusZoomLevel==this.map.getZoom())) {

					// clicked on selected country again - we deselect all and zoom out

					this.deselectActiveFeature();

					this.map.fitBounds(this.maxBounds);

					if (this.clearCountryList) this.clearCountryList();

				} else {

					this.selectFeatureLayer (layer);
					this.updateCountryList(isoA2, clickOnCountryListContacts);
				}
			};



			/**
			 * Calls *CountryList call back functions
			 */
			this.updateCountryList = function(isoA2, clickOnCountryListContacts)
			{
				if (clickOnCountryListContacts) {
					if (this.showCountryList) this.showCountryList(isoA2, null);
				} else {
					if (this.clearCountryList) this.clearCountryList();
				}
			}


			// --- Regions ---------------

			/**
			 * Show a region map for a given isoA2
			 */
			this.showRegionMap = function(isoA2)
			{
				this.isoA2 = isoA2 = isoA2 || this.isoA2;
				if (this.regionMap || !isoA2 || !this.options.regionsFeatures || !this.options.regionsFeatures[isoA2]) {
					return;
				}

				if (!this.regionMaps[isoA2]) {
					var regionFeature = this.options.regionsFeatures[isoA2];

					var that = this;
					var onEachFeature = function(feature, layer) {
						layer.on({
							mouseover: jQuery.proxy(that.highlightRegionEvent, that),
							mouseout: jQuery.proxy(that.resetHighlightRegionEvent, that),
							click: jQuery.proxy(that.clickRegionEvent, that)
						});

					};

					this.regionMaps[isoA2] = L.layerGroup();
					this.regionMaps[isoA2].addLayer(L.geoJson(regionFeature, {
							style: this.featureStyles.current.regionStyle,
							onEachFeature: onEachFeature
						})
					);
				}

				this.regionMap = this.regionMaps[isoA2];

				if (!this.regionMap) {
					return;
				}

				if (this.polygonHasFocus) {
					this.resetHighlightFeatureLayer(this.polygonHasFocus);
				}
				this.map.addLayer(this.regionMap);

				this.markerLayer.bringToFront();
			};


			/**
			 * Hide a region map when one is displayed
			 */
			this.hideRegionMap = function()
			{
				if (this.regionMap) {
					this.map.removeLayer(this.regionMap);
					this.regionMap = false;
					this.markerRegion = false;
					this.isoA2 = false;
				}
			};


			/**
			 * Event handler
			 * This is what happens when your mouse hovers over a region map element.
			 */
			this.highlightRegionEvent = function(e)
			{
				var layer = e.target;

				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					layer.setStyle(this.featureStyles.current.regionHoverStyle);

					if (L.Browser.svg) {
						var center = layer.getBounds().getCenter();
						this.markerRegion = L.circleMarker( center, {radius: 14, text: layer.feature.properties.name, fill:true, fillColor:'#ffffff', stroke: false, fillOpacity: 1} );

						this.regionMap.addLayer(this.markerRegion);
						this.markerRegion.bringToFront();
					}
				}
			};

			/**
			 * Event handler
			 * This is what happens when your mouse goes away from an region element.
			 */
			this.resetHighlightRegionEvent = function(e)
			{
				var layer = e.target;

				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					layer.setStyle(this.featureStyles.current.regionStyle);

					if (this.markerRegion) {
						this.regionMap.removeLayer(this.markerRegion);
						this.markerRegion = false;
					}
				}

			};

			/**
			 * Event handler
			 * Handles clicks on regions
			 */
			this.clickRegionEvent = function(e)
			{
				var layer = e.target;

				if (this.polygonHasFocus && (layer._leaflet_id==this.polygonHasFocus._leaflet_id)) {

					// just do nothing here

				} else {

					if (this.polygonHasFocus && this.polygonHasFocus.feature.properties.region) {
						this.polygonHasFocus.setStyle(this.featureStyles.current.regionStyle);
					}
					this.polygonHasFocus = layer;
					layer.setStyle(this.featureStyles.current.regionSelectStyle);

					if (this.showCountryList) this.showCountryList(layer.feature.properties.iso_a2, null, layer.feature.properties.region);

				}
			};



			// --- Marker -------------------

			var that = this;
			/**
			 * Event handler
			 * Handles clicks on marker
			 */
			this.clickMarkerEvent = function(e) {
				if (that.showCountryList) that.showCountryList(null, e.target.options.location_id);
			};


			// --- Map Events ---------------

			var startZoomLevel = 0;

			this.onZoomStart = function(event){
				that.startZoomLevel = that.map.getZoom();
			};

			this.onZoomEnd = function(){
				var zoomLevel = that.map.getZoom(),
					zoomOut = zoomLevel < that.startZoomLevel,
					zoomIn = zoomLevel >= that.startZoomLevel;

				// apply styles depending on zoom level

				if (that.map.getZoom()>1.0 && (that.map.getZoom()>=3.0 || that.options.countryCount == 1)) {
					that.featureStyles.current = that.featureStyles.hover;

				} else if (that.map.getZoom()<3.0) {
					that.featureStyles.current = that.featureStyles.base;
				}


				if (that.map.getZoom()>1.0 && (that.map.getZoom()>=3.0 || that.options.countryCount == 1)) {
					that.map.addLayer(that.tileLayer);
				} else {
					that.map.removeLayer(that.tileLayer)
					that.map.addLayer(that.countryLayer);
				}

				if(zoomLevel <= 3.0) {
					//that.map.panInsideBounds(that.maxBounds);

					that.hideRegionMap();
				} else {
					if (that.polygonHasFocus) {
						that.showRegionMap(that.polygonHasFocus.feature.properties.iso_a2);
					}
				}

				that.countryLayer.eachLayer(function (layer) {
						if (layer.feature.properties.is_active) {
							if (that.polygonHasFocus && (layer._leaflet_id==that.polygonHasFocus._leaflet_id)) {
								if (this.regionMap) {
									layer.setStyle(that.featureStyles.current.countryStyle);
								} else {
									layer.setStyle(that.featureStyles.current.countrySelectStyle);
								}

							} else if ((!that.polygonHasFocus) || (that.polygonHasFocus && (layer._leaflet_id!=that.polygonHasFocus._leaflet_id))) {
								layer.setStyle(that.featureStyles.current.countryStyle);
							}
						} else {
							layer.setStyle(that.featureStyles.current.countryNonactiveStyle);
						}
					}
				);

				that.markerLayer.bringToFront();

				// show and hide marker

				if (that.map.getZoom()>1.0 && (that.map.getZoom()>=3.0 || that.options.countryCount == 1)) {
					that.markerLayer.eachLayer(
						function (marker) {
							marker.options.clickable = true;
							if (marker._path) {
								var container = marker._path;
							} else {
								var container = marker._container;
							}
							L.DomUtil.addClass(container, 'leaflet-clickable');
							L.DomUtil.removeClass(container, 'leaflet-nonclickable');
							container.style['visibility'] = 'visible';
						}
					)
				} else {
					that.markerLayer.eachLayer(
						function (marker) {
							marker.options.clickable = false;
							if (marker._path) {
								var container = marker._path;
							} else {
								var container = marker._container;
							}
							L.DomUtil.addClass(container, 'leaflet-nonclickable');
							L.DomUtil.removeClass(container, 'leaflet-clickable');

							if (that.options.consultantTypeOnWorldmap[marker.options.location_type]) {
								container.style['visibility'] = 'visible';
							} else {
								container.style['visibility'] = 'hidden';
							}
						}
					)
				}
			};



			this.initializeMap = function(divId) {

				var that = this;

				// Map --------------------

				var southWest = new L.LatLng(-58.64489630449362, -201.42370526861555, true),
					northEast = new L.LatLng(81.29351994784489, 220.9293414555197, true);

				var southWest = new L.LatLng(-74.37648114088306, -202.03581113343313, true),
					northEast = new L.LatLng(86.40711375464741, 220.31723559070215, true);
				this.maxBounds = new L.LatLngBounds(southWest, northEast);

				// create a map in the 'map' div, set the view to a given place and zoom
				var minZoom = 1;
				this.map = L.map(divId, {attributionControl: false, minZoom: minZoom, maxZoom: this.options.maxZoom}).setView([35, 10], minZoom);


				// Country Layer ---------------

				this.countriesBounds = new L.LatLngBounds();

				this.franceBounds = new L.LatLngBounds([[41, -8],[52, 14]]);


				var onEachFeature = function(feature, layer) {
					if (layer.feature.properties.is_active) {

						// calculate bounds of the active countries
						if (layer.feature.properties.iso_a2 == 'FR') {
							that.countriesBounds.extend(that.franceBounds);
						} else {
							that.countriesBounds.extend(layer.getBounds());
						}

						layer.on({
							mouseover: jQuery.proxy(that.highlightFeatureEvent, that),
							mouseout: jQuery.proxy(that.resetHighlightFeatureEvent, that),
							click: jQuery.proxy(that.clickFeatureEvent, that)
						});
					} else {
						layer.setStyle(that.featureStyles.current.countryNonactiveStyle);
					};
				};

				this.countryLayer = L.geoJson(this.options.countryFeature,{
					style: this.featureStyles.current.countryStyle,
					onEachFeature: onEachFeature
				});


				// Tile Layer ---------------

				// look here: http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html

				var LayerGoogleCustom_styles = [
					{featureType: 'all', elementType: 'all', stylers: [{ saturation: -96 }]},
					{featureType: 'all', elementType: 'geometry', stylers: [ { gamma: 2.81 }]},
					{featureType: 'all', elementType: 'labels', stylers: [ { lightness: 30 }]},
					{featureType: 'road', elementType: 'labels', stylers: [ { visibility: 'off' }]},
					{featureType: 'transit', elementType: 'all', stylers: [ { saturation: 0, gamma:0, lightness:0 }]},
					{featureType: 'landscape', elementType: 'geometry', stylers: [ { lightness: 57 }]}
				];

				var LayerGoogleCustom_styles =
					[
					  {
						'featureType': 'all',
						'elementType': 'all',
						'stylers': [
						  { 'saturation': -96 }
						]
					  },{ 'featureType': 'water', 'stylers': [ { 'saturation': -100 }, { 'lightness': 73 } ]
					},{
						'elementType': 'geometry',
						'stylers': [
						  { 'gamma': 2.78 }
						]
					  },{
						'elementType': 'labels',
						'stylers': [
						  { 'lightness': 27 }
						]
					  },{
						'featureType': 'road',
						'elementType': 'labels',
						'stylers': [
						  { 'visibility': 'off' }
						]
					  },{
						'featureType': 'water',
						'stylers': [
							{ 'color': '#E3EAF1' },
							{'visibility': 'on' }
						]
					},{
						'featureType': 'landscape',
						'elementType': 'geometry',
						'stylers': [
						  { 'lightness': 47 }
						]
					  },{
						'featureType': 'administrative.province',
						'stylers': [
						  { 'visibility': 'off' }
						]
					  },{
						'featureType': 'administrative.country',
						'stylers': [
						  { 'gamma': 0.53 },
						  { 'hue': '#ff8000' },
						  { 'saturation': 58 }
						]
					  }
					];

				this.tileLayer = new L.Google('ROADMAP', {styles: LayerGoogleCustom_styles});


				// Marker ---------------

				this.markerLayer = L.featureGroup();
			};


			this.extendMap = function(callbackFunc)
			{
				callbackFunc.call(this);
			}


			// Initialize Map ---------------
			this.startMap = function()
			{

				var that = this;

				this.map.addLayer(this.countryLayer);



				// Marker ---------------

				this.map.addLayer(this.markerLayer);

				// disable marker at the beginning
				this.markerLayer.eachLayer(
					function (marker) {
						marker.options.clickable = false;
						if (marker._path) {
							var container = marker._path;
						} else {
							var container = marker._container;
						}
						L.DomUtil.addClass(container, 'leaflet-nonclickable');
						L.DomUtil.removeClass(container, 'leaflet-clickable');

						if (that.options.consultantTypeOnWorldmap[marker.options.location_type]) {
							container.style['visibility'] = 'visible';
						} else {
							container.style['visibility'] = 'hidden';
						}
					}
				);


				// Controls ---------------

				var InfoControl = L.Control.extend({
					options: {
						position: 'topright'
					},

					onAdd: function (map) {
						// create the control container with a particular class name
						this._div = L.DomUtil.create('div', 'info');

						this.update();
						return this._div;
					},

					// method that we will use to update the control based on feature properties passed
					update: function (props) {
						if (props && that.options.countryNames[props['iso_a2']]) {
							// This is called with when the mouse over an element.
							this._div.innerHTML = that.options.countryNames[props['iso_a2']];
							this._div.style.display = 'block';
						} else {
							this._div.innerHTML = '';
							this._div.style.display = 'none';
						}
					}

				});

				this.info = new InfoControl;
				this.map.addControl(this.info);



				// Events ---------------

				this.map.on('zoomstart', this.onZoomStart);
				this.map.on('zoomend',   this.onZoomEnd);


				// Init ---------------

				// fit map to country bounds when only some countries are selected
				if (this.options.countryCount>0 && this.options.countryCount < 50) {
					this.map.fitBounds(this.countriesBounds, {animate:false});
					// a little padding is used here otherwise rounding errors causing troubles
					this.map.setMaxBounds(this.map.getBounds().pad(0.01));
					this.maxBounds = this.map.getBounds().pad(-0.01);
				}

				if (this.options.countryCount == 1) {
					this.map.addLayer(this.tileLayer);
					this.showRegionMap(this.options.countryCodes[0]);
				}
				this.markerLayer.bringToFront();
				// this breaks stuff
				//this.map.setZoom(1.3);
			}

		}

