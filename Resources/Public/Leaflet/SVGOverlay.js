/*
 * L.SVGOverlay is used to overlay SVGs over the map (to specific geographical bounds).
 */

L.SVGOverlay = L.Class.extend({
	includes: L.Mixin.Events,

	options: {
		opacity: 1
	},

	initialize: function (url, bounds, options) { // (String, LatLngBounds, Object)
		this._url = url;
		this._bounds = L.latLngBounds(bounds);

		L.setOptions(this, options);
	},

	onAdd: function (map) {
		this._map = map;

		if (!this._svgroot) {
			this._initSVG();
		}

		map._panes.overlayPane.appendChild(this._svgroot);

		map.on('viewreset', this._reset, this);

		if (map.options.zoomAnimation && L.Browser.any3d) {
			map.on('zoomanim', this._animateZoom, this);
		}

		this._reset();
	},

	onRemove: function (map) {
		map.getPanes().overlayPane.removeChild(this._svgroot);

		map.off('viewreset', this._reset, this);

		if (map.options.zoomAnimation) {
			map.off('zoomanim', this._animateZoom, this);
		}
	},

	addTo: function (map) {
		map.addLayer(this);
		return this;
	},

	setOpacity: function (opacity) {
		this.options.opacity = opacity;
		this._updateOpacity();
		return this;
	},

	// TODO remove bringToFront/bringToBack duplication from TileLayer/Path
	bringToFront: function () {
		if (this._svgroot) {
			this._map._panes.overlayPane.appendChild(this._svgroot);
		}
		return this;
	},

	bringToBack: function () {
		var pane = this._map._panes.overlayPane;
		if (this._svgroot) {
			pane.insertBefore(this._svgroot, pane.firstChild);
		}
		return this;
	},

	svg: function () {
		return jQuery(this._svgroot).svg('get');
	},

	_initSVG: function () {
		this._svgroot = document.createElementNS(L.Path.SVG_NS, 'svg');

		this._updateOpacity();

		jQuery(this._svgroot).svg({
				loadURL: this._url,
				onLoad: L.bind(this._onImageLoad, this),
				addTo: true, changeSize: false,
	            settings: {}
			});
	},

	_animateZoom: function (e) {
		var map = this._map,
		    svgroot = this._svgroot,
		    scale = map.getZoomScale(e.zoom),
		    nw = this._bounds.getNorthWest(),
		    se = this._bounds.getSouthEast(),

		    topLeft = map._latLngToNewLayerPoint(nw, e.zoom, e.center),
		    size = map._latLngToNewLayerPoint(se, e.zoom, e.center)._subtract(topLeft),
		    origin = topLeft._add(size._multiplyBy((1 / 2) * (1 - 1 / scale)));

		svgroot.style[L.DomUtil.TRANSFORM] =
		        L.DomUtil.getTranslateString(origin) + ' scale(' + scale + ') ';
	},

	_reset: function () {
		var svgroot   = this._svgroot,
		    topLeft = this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
		    size = this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(topLeft);

		L.DomUtil.setPosition(svgroot, topLeft);

		svgroot.style.width  = size.x + 'px';
		svgroot.style.height = size.y + 'px';
	},

	_onImageLoad: function () {
		L.DomUtil.addClass(this._svgroot, 'leaflet-svg-layer');

		if (this._map.options.zoomAnimation && L.Browser.any3d) {
			L.DomUtil.addClass(this._svgroot, 'leaflet-zoom-animated')
		} else {
			L.DomUtil.addClass(this._svgroot, 'leaflet-zoom-hide')
		}
		this._updateOpacity();

		this.fire('load');
	},

	_updateOpacity: function () {
		this._svgroot.style.opacity = this.options.opacity;
	}
});

L.svgOverlay = function (url, bounds, options) {
	return new L.SVGOverlay(url, bounds, options);
};
