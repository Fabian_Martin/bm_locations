/*
 * L.SVGLayer is used to include SVG as layer (to specific geographical bounds).
 *
 * This layer does not test if the browser is capable of svg display.
 */

L.SVGLayer = L.Path.extend({
	includes: L.Mixin.Events,

	options: {
		opacity: 1,
		visibility: 1,
	},

	initialize: function (url, bounds, options) {
		L.Path.prototype.initialize.call(this, options);

		this._url = url;
		this._bounds = L.latLngBounds(bounds);
	},

	svg: function () {
		return this._svg;
	},

	getPathString: function () {
	},

	getBounds: function () {
		return this._bounds;
	},

	setOpacity: function (opacity) {
		this.options.opacity = opacity;
		this._updateOpacity();
		return this;
	},

	setVisibility: function (visibility) {
		this.options.visibility = visibility;
		this._updateVisibility();
		return this;
	},

	_initEvents: function () {
		L.Path.prototype._initEvents.call(this);

		this._map.on('viewreset', this._reset, this);

		this._reset();
	},

	_initStyle: function () {
		L.DomUtil.addClass(this._container, 'leaflet-svg-layer');
		this._updateOpacity();
		this._updateVisibility();
	},

	_initPath: function () {
		this._path = this._container = this._createElement('g');
		this._container.setAttribute('id', 'svglayer');

		this._SVGLoadXML(this._url);
	},

	_reset: function () {
		var topLeft = this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
			size = this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(topLeft);

		this._setPosition(this._container, topLeft);

		this._svg.setAttribute('width', size.x);
		this._svg.setAttribute('height', size.y);
	},

	_updateVisibility: function () {
		if (this._container) {
			this._container.setAttribute('visibility', (this.options.visibility ? 'visible' : 'hidden'));
		}
	},

	_updateOpacity: function () {
		if (this._container) {
			this._container.style.opacity = this.options.opacity;
		}
	},

	_updatePath: function () {
	},

	_SVGLoadXML: function (url) {
		var http = new XMLHttpRequest();
		http.open("GET", url, false);
		http.send();
		xmlBody = http.responseXML;

		var svg = xmlBody.getElementsByTagName("svg")[0];
		this._container.appendChild(svg);
		this._svg = svg;
	},

	// we need a modified setPosition function because of ie
	_setPosition: function (el, point, disable3D) { // (HTMLElement, Point[, Boolean])

		// jshint camelcase: false
		el._leaflet_pos = point;

		if (!disable3D && L.Browser.ie) {
			// transformation using style doesn't work in ie but as attribute
			el.setAttribute('transform', 'translate('+ point.x + ',' + point.y + ')');
		} else if (!disable3D && L.Browser.any3d) {
			el.style[L.DomUtil.TRANSFORM] =  L.DomUtil.getTranslateString(point);
		} else {
			el.style.left = point.x + 'px';
			el.style.top = point.y + 'px';
		}
	},



});

L.svglayer = function (url, bounds, options) {
	return new L.SVGLayer(url, bounds, options);
};
