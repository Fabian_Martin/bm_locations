/*!
 *
 *
 */

/*
 * L.Geocoding
 */

L.Geocoding = L.Control.extend({

    onAdd: function (map) {
        this._map = map;
        return L.DomUtil.create('div', 'leaflet-geocoding');
    }

    , onRemove: function (map) {
    }

	, getBounds: function () {
		return this._bounds;
	}

	, getLatLng: function () {
		return this._latlng;
	}

	, getFormattedAddress: function () {
		return this._formatted_address;
	}

    , geocode:function(query, callback) {
    	var that = this;

    	query = query + ',de';

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': query}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var res=results[0];
                that._formatted_address = res.formatted_address;
                that._latlng = new L.LatLng(res.geometry.location.lat(), res.geometry.location.lng());
                that._bounds = new L.LatLngBounds([
                        res.geometry.viewport.getSouthWest().lat(), res.geometry.viewport.getSouthWest().lng()
                        ], [
                        res.geometry.viewport.getNorthEast().lat(), res.geometry.viewport.getNorthEast().lng()
                    ]);
                status = true;
            } else {
            	that._formatted_address = '';
            	that._latlng = false;
            	that._bounds = false;
                status = false;
            }
			if (callback) {
				callback.call(that, status);
			}
        });

    }





});
