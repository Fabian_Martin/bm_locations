<html>
<head>
<script type="text/javascript" src="./getjson.js"></script>
<script type="text/javascript" src="./geojson2svg.js"></script>
</head>
<body>
	<h2>World Map</h2>
	<div id="mapArea" style="width: 800px; height: 800px;">
		<svg id="map" xmlns="http://www.w3.org/2000/svg" width="800"
			height="800" x="0" y="0">
      </svg>
	</div>
	<script type="text/javascript">

		<?php
		$dataURLPoly = file_get_contents('../ne_50m_admin_0_countries-15-good.json');
		?>

		var GeoJSON = <?php echo $dataURLPoly; ?>;

		drawGeoJSON(GeoJSON);

		function drawGeoJSON(geojson) {
			var svgMap = document.getElementById('map');
			var convertor = geojson2svg({
				width : 800,
				height : 800
			});
			var attributes = {
				'style' : 'stroke:#006600; fill: #F0F8FF;stroke-width:0.5px;',
				'vector-effect' : 'non-scaling-stroke'
			};
			var svgElements = convertor.convert(geojson, {
				attributes : attributes,
				output : 'svg',
				explode : false
			});
			var parser = new DOMParser();
			svgElements.forEach(function(svgStr) {
				var svg = parseSVG(svgStr);
				svgMap.appendChild(svg);
			});
		}
		//parseSVG from http://stackoverflow.com/questions/3642035/jquerys-append-not-working-with-svg-element
		function parseSVG(s) {
			var div = document.createElementNS('http://www.w3.org/1999/xhtml',
					'div');
			div.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg">' + s
					+ '</svg>';
			var frag = document.createDocumentFragment();
			while (div.firstChild.firstChild)
				frag.appendChild(div.firstChild.firstChild);
			return frag;
		}
	</script>
</body>
</html>
