#!/bin/sh


#topojson --id-property iso_a2 -p iso_a3 -p iso_n3 -p name -p iso_a2 --simplify 0.000005 -o ne_50m_admin_0_countries.topojson ne_50m_admin_0_countries.shp
topojson   --cartesian --allow-empty  --id-property iso_a2 -p iso_a3 -p iso_n3 -p name -p iso_a2 -o ne_50m_admin_0_countries.topojson ne_50m_admin_0_countries.shp

geojson --id-property iso_a2  ne_50m_admin_0_countries.topojson
sed 's/ //' ne_50m_admin_0_countries.json > ne_50m_admin_0_countries.geojson 
rm ne_50m_admin_0_countries.json 


