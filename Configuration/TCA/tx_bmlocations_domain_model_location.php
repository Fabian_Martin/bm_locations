<?php
return [
    'ctrl' => [
        'title'	=> 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location',
        'label' => 'longitude',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
		'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
		'searchFields' => 'longitude,latitude,adress,zip,city,auto_coordinates,hide_location,hide_adress,zip_ranges,type,no_country_list_in_pdf,segment,region_name,competence,region,consultanttype,country,contact,include_map_js,coordinates',
        'iconfile' => 'EXT:bm_locations/Resources/Public/Icons/tx_bmlocations_domain_model_location.gif'
    ],
    'interface' => [
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, include_map_js, coordinates longitude, latitude, adress, zip, city, auto_coordinates, hide_location, hide_adress, zip_ranges, type, no_country_list_in_pdf, segment, region_name, competence, region, consultanttype, country, contact',
    ],
    'types' => [
		'1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, include_map_js, coordinates, longitude, latitude, adress, zip, city, auto_coordinates, hide_location, hide_adress, zip_ranges, type, no_country_list_in_pdf, segment, region_name, competence, region, consultanttype, country, contact, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
		'sys_language_uid' => [
			'exclude' => true,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'special' => 'languages',
				'items' => [
					[
						'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
						-1,
						'flags-multiple'
					]
				],
				'default' => 0,
			],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_bmlocations_domain_model_location',
                'foreign_table_where' => 'AND tx_bmlocations_domain_model_location.pid=###CURRENT_PID### AND tx_bmlocations_domain_model_location.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
		't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
		'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
		'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        "include_map_js" => Array ( // this is a pseudo field
            "exclude" => 0,
            "label" => '',
            "l10n_mode" => "exclude",
            "config" => Array (
                "type" => 'select',
                'renderType' => 'map',
                #'userFunc' => 'EXT:bm_locations/Classes/Utility/Geo/TCEforms.php:&\Bitmotion\BmLocations\Utility\Geo\TCEforms->GetMapJavaScript',
            ),
        ),

        'longitude' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.longitude',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim'
			],
	    ],
	    'latitude' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.latitude',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim'
			],
	    ],
	    'adress' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.adress',
	        'config' => [
			    'type' => 'text',
			    'cols' => 40,
			    'rows' => 15,
			    'eval' => 'trim'
			]
	    ],
	    'zip' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.zip',
	        'config' => [
			    'type' => 'input',
			    'size' => 4,
			    'eval' => 'int'
			]
	    ],
	    'city' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.city',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim'
			],
	    ],
	    'auto_coordinates' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.auto_coordinates',
	        'config' => [
			    'type' => 'check',
			    'items' => [
			        '1' => [
			            '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
			        ]
			    ],
			    'default' => 0
			]
	    ],
	    'hide_location' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.hide_location',
	        'config' => [
			    'type' => 'check',
			    'items' => [
			        '1' => [
			            '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
			        ]
			    ],
			    'default' => 0
			]
	    ],
	    'hide_adress' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.hide_adress',
	        'config' => [
			    'type' => 'check',
			    'items' => [
			        '1' => [
			            '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
			        ]
			    ],
			    'default' => 0
			]
	    ],
	    'zip_ranges' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.zip_ranges',
	        'config' => [
			    'type' => 'text',
			    'cols' => 40,
			    'rows' => 15,
			    'eval' => 'trim'
			]
	    ],
	    'type' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.type',
	        'config' => [
			    'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xml:tx_bmlocations_domain_model_location.type.address', 0],
                    ['LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xml:tx_bmlocations_domain_model_location.type.reference', 1],
                ],
                'size' => 1,
                'maxitems' => 1
			],
	    ],
	    'no_country_list_in_pdf' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.no_country_list_in_pdf',
	        'config' => [
			    'type' => 'check',
			    'items' => [
			        '1' => [
			            '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
			        ]
			    ],
			    'default' => 0
			]
	    ],
	    'segment' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.segment',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim'
			],
	    ],
	    'region_name' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.region_name',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim'
			],
	    ],
	    'competence' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.competence',
	        'config' => [
			    'type' => 'select',
                'renderType' => 'selectSingle',
			    'foreign_table' => 'tx_bmlocations_domain_model_competence',
			    'minitems' => 0,
			    'maxitems' => 1,
			],
	    ],
	    'region' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.region',
	        'config' => [
			    'type' => 'select',
			    'renderType' => 'selectMultipleSideBySide',
			    'foreign_table' => 'tx_bmlocations_domain_model_region',
			    'MM' => 'tx_bmlocations_location_region_mm',
			    'size' => 10,
			    'autoSizeMax' => 30,
			    'maxitems' => 9999,
			    'multiple' => 0,
                'fieldControls' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
			],
	    ],
	    'consultanttype' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.consultanttype',
	        'config' => [
			    'type' => 'select',
                'renderType' => 'selectSingle',
			    'foreign_table' => 'tx_bmlocations_domain_model_consultanttype',
			    'minitems' => 0,
			    'maxitems' => 1,
			],
	    ],
	    'country' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.country',
	        'config' => [
			    'type' => 'select',
                'renderType' => 'selectSingle',
			    'foreign_table' => 'static_countries',
			    'minitems' => 0,
			    'maxitems' => 1,
			],
	    ],
	    'contact' => [
	        'exclude' => true,
	        'label' => 'LLL:EXT:bm_locations/Resources/Private/Language/locallang_db.xlf:tx_bmlocations_domain_model_location.contact',
	        'config' => [
			    'type' => 'inline',
			    'foreign_table' => 'tx_bmlocations_domain_model_contact',
			    'foreign_field' => 'location',
			    'maxitems' => 9999,
			    'appearance' => [
			        'collapseAll' => 0,
			        'levelLinksPosition' => 'top',
			        'showSynchronizationLink' => 1,
			        'showPossibleLocalizationRecords' => 1,
			        'showAllLocalizationLink' => 1
			    ],
			],
	    ],
        'consultant' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
