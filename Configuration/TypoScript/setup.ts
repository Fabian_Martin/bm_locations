plugin.tx_bmlocations_map {
  view {
    templateRootPaths.0 = EXT:bm_locations/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_bmlocations_map.view.templateRootPath}
    partialRootPaths.0 = EXT:bm_locations/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_bmlocations_map.view.partialRootPath}
    layoutRootPaths.0 = EXT:bm_locations/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_bmlocations_map.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_bmlocations_map.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

page.includeJS.14934648783 =

plugin.tx_bmlocations_list {
  view {
    templateRootPaths.0 = EXT:bm_locations/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_bmlocations_list.view.templateRootPath}
    partialRootPaths.0 = EXT:bm_locations/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_bmlocations_list.view.partialRootPath}
    layoutRootPaths.0 = EXT:bm_locations/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_bmlocations_list.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_bmlocations_list.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}

plugin.tx_bmlocations_pdfexport {
  view {
    templateRootPaths.0 = EXT:bm_locations/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_bmlocations_pdfexport.view.templateRootPath}
    partialRootPaths.0 = EXT:bm_locations/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_bmlocations_pdfexport.view.partialRootPath}
    layoutRootPaths.0 = EXT:bm_locations/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_bmlocations_pdfexport.view.layoutRootPath}
  }
  persistence {
    storagePid = {$plugin.tx_bmlocations_pdfexport.persistence.storagePid}
    #recursive = 1
  }
  features {
    #skipDefaultArguments = 1
  }
  mvc {
    #callDefaultActionIfActionCantBeResolved = 1
  }
}