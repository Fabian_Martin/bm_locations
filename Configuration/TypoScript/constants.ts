
plugin.tx_bmlocations_map {
  view {
    # cat=plugin.tx_bmlocations_map/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:bm_locations/Resources/Private/Templates/
    # cat=plugin.tx_bmlocations_map/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:bm_locations/Resources/Private/Partials/
    # cat=plugin.tx_bmlocations_map/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:bm_locations/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_bmlocations_map//a; type=string; label=Default storage PID
    storagePid =
  }
}

plugin.tx_bmlocations_list {
  view {
    # cat=plugin.tx_bmlocations_list/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:bm_locations/Resources/Private/Templates/
    # cat=plugin.tx_bmlocations_list/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:bm_locations/Resources/Private/Partials/
    # cat=plugin.tx_bmlocations_list/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:bm_locations/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_bmlocations_list//a; type=string; label=Default storage PID
    storagePid =
  }
}

plugin.tx_bmlocations_pdfexport {
  view {
    # cat=plugin.tx_bmlocations_pdfexport/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:bm_locations/Resources/Private/Templates/
    # cat=plugin.tx_bmlocations_pdfexport/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:bm_locations/Resources/Private/Partials/
    # cat=plugin.tx_bmlocations_pdfexport/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:bm_locations/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_bmlocations_pdfexport//a; type=string; label=Default storage PID
    storagePid =
  }
}
