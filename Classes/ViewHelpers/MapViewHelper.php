<?php

namespace Bitmotion\BmLocations\ViewHelpers;

class MapViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {




	/**
	 * @param \Bitmotion\BmLocations\ViewHelpers\Location[] $markerLocations
	 * @param array $countryList
	 * @param array $regionList
	 * @param \Bitmotion\BmLocations\ViewHelpers\ConsultantType[] $legend
	 * @param array $settings
	 * @return string
	 */
	public function render($markerLocations, $countryList, $regionList, $legend, $settings)
	{
		$strPathToResources = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->controllerContext->getRequest()->getControllerExtensionKey()) . 'Resources/';
		$strPathToImages = $strPathToResources . 'Public/Images/';
		$strPathToIcons = $strPathToResources . 'Public/Icons/';
		$strPathToLeaflet = $strPathToResources . 'Public/Leaflet/';
		$strPathToJquerySVG = $strPathToResources . 'Public/jquery-svg/';


		$countryCount = count($countryList);
		$countryListJson = json_encode($countryList);
		$countryCodesJson = json_encode(array_keys($countryList));

		$regionCount = count($regionList);
		$regionListJson = json_encode($regionList);


		// countries map

		#$strCountries = file_get_contents($strPathToResources . 'Private/Map/ne_50m_admin_0_countries-100good.json');

		// this is simplified with mapshaper.org from the above file

		// this works in ie8
		$strCountries = file_get_contents($strPathToResources . 'Private/Map/ne_50m_admin_0_countries-15-good.json');

		#$strCountries = file_get_contents($strPathToResources . 'Private/Map/ne_50m_admin_0_countries-55-good.json');
		#$strCountries = file_get_contents($strPathToResources . 'Private/Map/ne_50m_admin_0_countries-70-good.json');

		$objCountries = json_decode($strCountries);



		// activate countries

		foreach ($countryList as $isoA2 => $name) {
			foreach ($objCountries->features as $i => $value) {

				if ($objCountries->features[$i]->properties->iso_a2 == $isoA2) {
					$objCountries->features[$i]->properties->is_active = true;
				}
			}
		}
		$strCountries = json_encode($objCountries);


		// marker

		$strMarker = '';
		foreach ($markerLocations as $location) {
			/* @var $location \Bitmotion\BmLocations\ViewHelpers\Location */

			if ('' == $location->getLatitude()) {
				continue;
			}
			if ('' == $location->getLongitude()) {
				continue;
			}
			if (0.0 == (float)$location->getLatitude() AND 0.0 == (float)$location->getLongitude()) {
				continue;
			}

			$color = '#ff0000';
			if ($consultantType = $location->getConsultantType()) {
				$color = $consultantType->getColor();
			}

			$labelContent = "{$location->getConsultant()->getName()}, {$location->getCity()}";

			$strMarker .= "\n				this.markerLayer.addLayer(L.circle([{$location->getLatitude()}, {$location->getLongitude()}] , 40000, {location_type: {$location->getConsultantType()->getUid()}, location_id: {$location->getUid()}, clickable: true, fillColor: '{$color}', color: '{$color}', weight:0, fillOpacity:1.0 }).bindLabel(" . json_encode($labelContent) . ").addTo(this.map).on('click', this.clickMarker));";
		}




		$strMap = "

		L.Circle = L.Circle.extend({
			projectLatlngs: function () {
				var lngRadius = this._getLngRadius(),
						latlng2 = new L.LatLng(this._latlng.lat, this._latlng.lng - lngRadius),
						point2 = this._map.latLngToLayerPoint(latlng2);

				this._point = this._map.latLngToLayerPoint(this._latlng);

				zoom = this._map.getZoom();

				this._radius = 1 * zoom;
			},
		});



		function LocationMap() {

			this.map;

			// State handling ---------

			this.polygonHasFocus = false;


			// General data ---------


			this.countryNames = {$countryListJson};
			this.countryCodes = {$countryCodesJson};
			this.countryCount = {$countryCount};

			this.maxBound;

			this.info;

			// Country Layer ---------------

			this.countryFeature = {$strCountries};

			this.countryStyle = {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '{$settings['countryFillColor']}'
			};
			this.countryNonactiveStyle = {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '{$settings['countryNonactiveFillColor']}',
					clickable: false
			};
			this.countryHoverStyle = {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '{$settings['countryHoverFillColor']}'
			};
			this.countrySelectStyle = {
					color: '#ffffff',
					weight: 1,
					opacity: 1,
					fillOpacity: 1.0,
					fillColor: '{$settings['countrySelectFillColor']}'
			};

			jQuery('<style>.leaflet-svg-layer .area:hover, #svglayer .area:hover { fill: {$settings['countryHoverFillColor']}; }</style>').appendTo('head');

			// Country Layer ---------------

			this.countriesBounds;

			this.franceBounds;

			this.countryLayer;


			// local svg map ---------------

			this.regionMap;
			this.regionMaps = {$regionListJson};


			// Marker ---------------

			this.markerLayer;


			// Methods ---------------

			// trigger a 'click' by iso code
			this.clickCountryByID = function(id) {
				layers = this.countryLayer.getLayers();
				for (var i in layers) {
					layer = layers[i];
					if (layer.feature.properties.iso_a2 == id && !this.isActiveLayer(layer)) {
						layer.fire('click');
						return;
					}
				}
			};


			this.isActiveLayer = function(layer) {
				if (this.polygonHasFocus && (layer._leaflet_id==this.polygonHasFocus._leaflet_id)) {
					return true;
				}
				return false;
			};


			// This is what happens when your mouse hovers over a map element.
			this.highlightFeature = function(e) {
				var layer = e.target;

				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					layer.setStyle(this.countryHoverStyle);
					this.info.update(layer.feature.properties);
				}
			};


			// This is what happens when your mouse goes away from an element.
			this.resetHighlight = function(e) {
				var layer = e.target;

				if ((!this.polygonHasFocus) || (this.polygonHasFocus && (layer._leaflet_id!=this.polygonHasFocus._leaflet_id))) {
					this.countryLayer.resetStyle(layer);
				}

				if (this.polygonHasFocus) {
					this.info.update(this.polygonHasFocus.feature.properties);
				} else {
					this.info.update();
				}
			};


			this.clickFeature = function(e) {
				var layer = e.target;

				if (this.polygonHasFocus && (layer._leaflet_id==this.polygonHasFocus._leaflet_id)) {
					this.countryLayer.resetStyle(this.polygonHasFocus);
					this.info.update();

					var isoA2 = this.polygonHasFocus.feature.properties.iso_a2;
					if (this.regionMaps[isoA2]) {
						this.regionMaps[isoA2].setVisibility(0);
					}
					this.map.fitBounds(this.maxBounds);
					setCountrySelectbox('');
					this.polygonHasFocus = false;

				} else {

					if (this.polygonHasFocus) {
						this.countryLayer.resetStyle(this.polygonHasFocus);

						var isoA2 = this.polygonHasFocus.feature.properties.iso_a2;
						if (this.regionMaps[isoA2]) {
							this.regionMaps[isoA2].setVisibility(0);
						}
					}
					if (layer.feature.properties.iso_a2 == 'FR') {
						var bounds = this.franceBounds;
					} else {
						var bounds = layer.getBounds();
					}
					this.polygonHasFocus = layer;
					this.map.fitBounds(bounds);

					this.info.update(layer.feature.properties);

					var isoA2 = layer.feature.properties.iso_a2;
					setCountrySelectbox(isoA2);

					if (this.regionMaps[isoA2]) {
						this.countryLayer.resetStyle(layer);
						this.showRegionMap(isoA2)
						this.markerLayer.bringToFront();
						clearCountryList();

					} else {
						layer.setStyle(this.countrySelectStyle);

						if (!bd_locations_svg) {
							if (showCountryImageMap(isoA2)) {
								return;
							}
						}

						showCountryList(isoA2, null);
					}
				}
			};


			this.showRegionMap = function(isoA2) {

				regionMap = this.regionMaps[isoA2];

				if (!regionMap) {
					return;
				}

				this.map.addLayer(regionMap);

				var els = document.getElementsByClassName('area');
				for (i = 0; i < els.length; i++) {
				  els[i].addEventListener('click',this.clickRegion,false);
				}
				regionMap.setVisibility(1);
			};


			this.clickRegion = function(e) {

				var svgHasClass = function(el, name) {
					return (el.className.baseVal.length > 0) &&
					        new RegExp('(^|\\s)' + name + '(\\s|$)').test(el.className.baseVal);
				};

				if (svgHasClass(e.target, 'area')) {
					var g = e.target;
				} else if (svgHasClass(e.target.parentNode, 'area')) {
					var g = e.target.parentNode;
				} else {
					return;
				}

				showCountryList(null, null, g.id);
			};


			this.clickMarker = function(e) {
				showCountryList(null, e.target.options.location_id);
			};


			// Events ---------------

			var startZoomLevel = 0;

			this.onZoomStart = function(event){
				this.startZoomLevel = this.map.getZoom();
			};

			this.onZoomEnd = function(){
				var zoomLevel = this.map.getZoom();

				var zoomOut = zoomLevel < this.startZoomLevel;
				var zoomIn = zoomLevel >= this.startZoomLevel;

				if (zoomOut && this.regionMap) {
					if (this.polygonHasFocus) {
						this.polygonHasFocus = false;
						this.countryLayer.resetStyle(this.polygonHasFocus);
						this.info.update();
					}
					this.regionMap.setVisibility(0);
				}

				if(zoomLevel <= 2.0) {
					//this.map.panInsideBounds(this.maxBounds);
				} else {
					//this.markerLayer.bringToFront();
				};

				if(zoomLevel >= 3.0) {
					this.markerLayer.eachLayer(
						function (marker) {
							marker.options.clickable = true;
							if (marker._path) {
								var container = marker._path;
							} else {
								var container = marker._container;
							}
							L.DomUtil.addClass(container, 'leaflet-clickable');
							L.DomUtil.removeClass(container, 'leaflet-nonclickable');
						}
					)
				};
				if(zoomLevel < 3.0) {
					this.markerLayer.eachLayer(
						function (marker) {
							marker.options.clickable = false;
							if (marker._path) {
								var container = marker._path;
							} else {
								var container = marker._container;
							}
							L.DomUtil.addClass(container, 'leaflet-nonclickable');
							L.DomUtil.removeClass(container, 'leaflet-clickable');
						}
					)
				};
			};


			this.Initialize = function() {

				// Map --------------------

				var southWest = new L.LatLng(-58.64489630449362, -201.42370526861555, true);
				var northEast = new L.LatLng(81.29351994784489, 220.9293414555197, true);

				var southWest = new L.LatLng(-74.37648114088306, -202.03581113343313, true);
				var northEast = new L.LatLng(86.40711375464741, 220.31723559070215, true);
				this.maxBounds = new L.LatLngBounds(southWest, northEast);

				// create a map in the 'map' div, set the view to a given place and zoom
				var minZoom = 1;
				this.map = L.map('map-view-helper', {attributionControl: false, minZoom: minZoom}).setView([35, 10], minZoom);


				if (bd_locations_svg && L.Browser.svg && {$regionCount}) {
					for( var isoA2 in this.regionMaps ) {
						var regionMapData = this.regionMaps[isoA2];
						var	overlaybounds = new L.LatLngBounds(
							new L.LatLng(regionMapData.s, regionMapData.w),
							new L.LatLng(regionMapData.n, regionMapData.e));
						var regionMap = new L.SVGLayer(regionMapData.url, overlaybounds, {
								opacity: 1
							});
						this.regionMaps[isoA2] = regionMap;
					}

				} else {
					this.regionMaps = [];
				}

				// Controls ---------------

				var that = this;
				var InfoControl = L.Control.extend({
				    options: {
				        position: 'topright'
				    },

				    onAdd: function (map) {
				        // create the control container with a particular class name
				        this._div = L.DomUtil.create('div', 'info');

						this.update();
						return this._div;
				    },

					// method that we will use to update the control based on feature properties passed
				    update: function (props) {
						if (props && that.countryNames[props['iso_a2']]) {
							// This is called with when the mouse over an element.
							this._div.innerHTML = that.countryNames[props['iso_a2']];
							this._div.style.display = 'block';
						} else {
							this._div.innerHTML = '';
							this._div.style.display = 'none';
						}
					}

				});

				this.info = new InfoControl;
				this.map.addControl(this.info);


				// Country Layer ---------------

				this.countriesBounds = new L.LatLngBounds();

				this.franceBounds = new L.LatLngBounds([[41, -8],[52, 14]]);

				var onEachFeature = function(feature, layer) {
					if (layer.feature.properties.is_active) {

						// calculate bounds of the active countries
						if (layer.feature.properties.iso_a2 == 'FR') {
							this.countriesBounds.extend(this.franceBounds);
						} else {
							this.countriesBounds.extend(layer.getBounds());
						}

						layer.on({
							mouseover: jQuery.proxy(this.highlightFeature, this),
							mouseout: jQuery.proxy(this.resetHighlight, this),
							click: jQuery.proxy(this.clickFeature, this)
						});
					} else {
						layer.setStyle(this.countryNonactiveStyle);
					};
				};

				this.countryLayer = L.geoJson(this.countryFeature,{
					style: this.countryStyle,
					onEachFeature: jQuery.proxy(onEachFeature, this)
				});
				this.map.addLayer(this.countryLayer);


				// Marker ---------------

				this.markerLayer = L.featureGroup();
				this.map.addLayer(this.markerLayer);

				{$strMarker}


				// disable marker at the beginning
				this.markerLayer.eachLayer(
					function (marker) {
						marker.options.clickable = false;
						if (marker._path) {
							var container = marker._path;
						} else {
							var container = marker._container;
						}
						L.DomUtil.addClass(container, 'leaflet-nonclickable');
						L.DomUtil.removeClass(container, 'leaflet-clickable');

					}
				);


				// Events ---------------

				this.map.on('zoomstart', jQuery.proxy(this.onZoomStart, this));
				this.map.on('zoomend', jQuery.proxy(this.onZoomEnd, this));


				// Initialize ---------------

				// fit map to country bounds when only some countries are selected
				if (this.countryCount>0 && this.countryCount < 50) {
					this.map.fitBounds(this.countriesBounds, {animate:false});
					// a little padding is used here otherwise rounding errors causing troubles
					this.map.setMaxBounds(this.map.getBounds().pad(0.01));
					this.maxBounds = this.map.getBounds().pad(-0.01);
				}

				if (this.countryCount == 1) {
					this.showRegionMap(this.countryCodes[0]);
					this.markerLayer.bringToFront();
				}


			}

		};


		";


        $strToReturn = '';

		$strToReturn .= '
			<script type="text/javascript">
			var bd_locations_svg = true;
			</script>
			<!--[if lte IE 10]>
			<script type="text/javascript">
			var bd_locations_svg = false;
			</script>
			<![endif]-->
			';
		$strToReturn .= '<script type="text/javascript">' . ($strMap) . '</script>';

		if (false) {

			$strToReturn .= '<script type="text/javascript">'."

				function CalcBoundsCenter(bounds) {

					var sw = map.project(bounds.getSouthWest());
					var ne = map.project(bounds.getNorthEast());

					var center = new L.point(
					        (sw.x + ne.x) / 2,
					        (sw.y + ne.y) / 2);

					 var target = map.unproject(center);

					 return target;
				};


				function showMapInfo(bounds) {

					var info = '';
					info = info + 'Zoom: ' + map.getZoom() + ' ('+map.getMinZoom()+'-'+map.getMaxZoom()+')';
					info = info + '<br />';
					info = info + 'Center: ' + map.getCenter().lat + ', ' +  map.getCenter().lng;
					info = info + '<br />';
					info = info + 'SouthWest: ' + bounds.getSouthWest().lat + ', ' +  bounds.getSouthWest().lng;
					info = info + '<br />';
					info = info + 'NorthEast: ' + bounds.getNorthEast().lat + ', ' +  bounds.getNorthEast().lng;
					info = info + '<br />';
					info = info + 'Bounds Center: ' + bounds.getCenter().lat + ', ' +  bounds.getCenter().lng;
					document.getElementById('map-view-info').innerHTML = info;


					var marker = L.marker([CalcBoundsCenter(bounds).lat,CalcBoundsCenter(bounds).lng]).addTo(map);
					var polygon = L.polygon([bounds.getSouthWest(),bounds.getNorthWest(),bounds.getNorthEast(),bounds.getSouthEast(),bounds.getSouthWest()]).addTo(map);
				}

				function setToBounds(bounds) {
					map.fitBounds(bounds);
				}
				</script>";


			$strToReturn .= '
					<div id="map-view-info" style="font-size:90%;height:15em;border:1px #888 solid;padding:0.5em"></div>
					<input type="button" onclick="showMapInfo(map.getBounds())" value="Info Map" />
					<input type="button" onclick="showMapInfo(this.maxBounds)" value="Info this.maxBounds" />
					<input type="button" onclick="showMapInfo(countriesBounds)" value="Info Country Bound" />
					<input type="button" onclick="setToBounds(countriesBounds)" value="Zoom to Country Bounds" />
					<input type="button" onclick="setToBounds(map.getBounds())" value="Zoom to Map Bounds" />
					<input type="button" onclick="setToBounds(this.maxBounds)" value="Zoom to this.maxBounds" />
					';
		}


		$this->controllerContext->getResponse()->addAdditionalHeaderData('<link rel="stylesheet" href="'.$strPathToLeaflet.'leaflet.css" />
<!--[if lte IE 8]>
	<link rel="stylesheet" href="'.$strPathToLeaflet.'leaflet.ie.css" />
<![endif]-->');


		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>');

		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToResources . 'Public/JavaScript/jquery.maphilight.min.js"></script>');

 		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToJquerySVG.'jquery.svg.js"></script>');
// 		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToJquerySVG.'jquery.svgdom.js"></script>');
// 		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToJquerySVG.'jquery.svganim.js"></script>');
		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'leaflet-src.js"></script>');
//		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'leaflet-providers.js"></script>');
		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'leaflet-geocoding-custom.js"></script>');
		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'leaflet.label.js"></script>');
//		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'SVGOverlay.js"></script>');
		$this->controllerContext->getResponse()->addAdditionalHeaderData(' <script src="'.$strPathToLeaflet.'SVGLayer.js"></script>');

		return $strToReturn;

	}

}

