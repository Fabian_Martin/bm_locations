<?php

namespace Bitmotion\BmLocations\ViewHelpers;

use Bitmotion\BmLocations\Domain\Model\ConsultantType;
use Bitmotion\BmLocations\Domain\Model\Location;
use Doctrine\Common\Util\Debug;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentValueException;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class MapGoogleViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    /**
     * @param string $mapId
     * @param array $markerLocations
     * @param array $countryList
     * @param ConsultantType[] $legend
     * @param array $settings
     * @return string
     * @throws InvalidArgumentValueException
     */
	public function render($mapId, $markerLocations, $countryList, $legend, $settings)
	{
		$strPathToResources = ExtensionManagementUtility::siteRelPath('bm_locations') . 'Resources/';
		$strPathToLeaflet = $strPathToResources . 'Public/Leaflet/';

		if ($settings['displayRegions'] AND $settings['division']) {

			switch ($settings['division']) {
				case 0:
					// Alle
					$division = 'schwein';
					break;
                case 1:
                    throw new InvalidArgumentValueException('Division Geflügelhaltung is not supported anymore.');
                    break;
                case 2:
					// Schwein
					$division = 'schwein';
					break;
				case 3:
					// Legehennenhaltung
					$division = 'legehennen';
					break;
				case 4:
					// Geflügelmast
					$division = 'gefluegel';
					break;
				default:
                    throw new InvalidArgumentValueException('No division selected.');
			}

			if ($_GET['abs']) {
			    $svg2json = new \Bitmotion\BmLocations\Utility\Geo\Svg2GeoJson();
				$countriesRegions = $svg2json->convert($strPathToResources . 'Private/Map/world_75-cleaned2-abs.svg', '//svg:g[@id="'.$division.'"]/svg:g/svg:path');
			} else {
                $svg2json = new \Bitmotion\BmLocations\Utility\Geo\Svg2GeoJson();
				$countriesRegions = $svg2json->convert($strPathToResources . 'Private/Map/world_75-cleaned3.svg', '//svg:g[@id="'.$division.'"]/svg:g/svg:path');
			}
		}

		// this works in ie8 - others are too much data
		$strCountries = file_get_contents($strPathToResources . 'Private/Map/ne_50m_admin_0_countries-15-good.json');
		$objCountries = json_decode($strCountries);

		// activate countries
		foreach ($countryList as $isoA2 => $name) {
			foreach ($objCountries->features as $i => $value) {
				if ($objCountries->features[$i]->properties->iso_a2 == $isoA2) {
					$objCountries->features[$i]->properties->is_active = true;
				}
			}
		}

		// marker
		$strMarker = '';
		foreach ($markerLocations as $location) {
			/* @var $location Location */
			DebuggerUtility::var_dump($location);

			if (!$location['consultanttype']) {
				continue;
			}

			if ('' == $location['latitude']) {
				continue;
			}
			if ('' == $location['longitude']) {
				continue;
			}
			if (0.0 == (float)$location['latitude'] && 0.0 == (float)$location['longitude']) {
				continue;
			}

			$color = '#ff0000';
			if ($consultantType = $location['consultanttype']) {
			    //TODO Get Color from consultanttype
				$color = 'blue';
			}

			//TODO Get Consultantname
            /**TODO $location->getConsultant()->getName() */
			$labelContent = "{'Name'}, {$location['city']}";

			$strMarker .= "\n				this.markerLayer.addLayer(L.circle([{$location['latitude']}, {$location['longitude']}] , 40000, {location_type: {$location['consultanttype']}, location_id: {$location['uid']}, clickable: true, fillColor: '{$color}', color: '{$color}', weight:0, fillOpacity:1.0 }).bindLabel(" . json_encode($labelContent) . ").addTo(this.map).on('click', this.clickMarkerEvent));";
		}


		// collect data and settings for js

		$options = $settings;
		$options['countryNames'] = $countryList;
		$options['countryCodes'] = array_keys($countryList);
		$options['countryCount'] = count($countryList);
		$options['countryFeature'] = $objCountries;
		$options['regionsFeatures'] = $countriesRegions;
		// cleanup for json
		foreach ($options as $key => $value) {
			if ($value == "0") {
				$options[$key] = false;
			}
		}


        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'ie'] = '<link rel="stylesheet" href="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'leaflet.css').'" />
<!--[if lte IE 8]>
	<link rel="stylesheet" href="'.$strPathToLeaflet.'leaflet.ie.css" />
<![endif]-->';

        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'1'] = '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiVxv7jAjLAUo8ELKzE5obzduZETbfuAE"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'2'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'src/Leaflet.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'3'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'leaflet-src.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'4'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'leaflet-geocoding-custom.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'5'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'leaflet.label.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'6'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToLeaflet.'GoogleCustom.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'7'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToResources.'Public/JavaScript/MapGoogleViewHelper.js').'"></script>';
        $GLOBALS['TSFE']->additionalHeaderData['BmLocations'.'8'] = '<script src="'.GeneralUtility::createVersionNumberedFilename($strPathToResources.'Public/JavaScript/MapGoogleViewHelper.js').'"></script>';

        $strToReturn = '';
		$strToReturn .= '
			<script type="text/javascript">'."
				myMap12334 = new LocationMap(".json_encode($options).");
				myMap12334.initializeMap('{$mapId}');
				myMap12334.extendMap(function() {
						{$strMarker}
					}
				);
				myMap12334.startMap();
			</script>";


		return $strToReturn;
	}
}

