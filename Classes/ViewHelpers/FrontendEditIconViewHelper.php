<?php

namespace Bitmotion\BmLocations\ViewHelpers;

use \TYPO3\CMS\Core\Utility\GeneralUtility;

class FrontendEditIconViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


	/**
	 * Creates edit icon for FE editing
	 *
	 * @param \Bitmotion\BmLocations\Domain\Model\Location $record Tx_Extbase_DomainObject_AbstractEntity
 	 * @return string
	 */
	public function render($record)
	{
		if (!$GLOBALS['TSFE']->beUserLogin){
			return '';
		}

		$tablename = strtolower(get_class($record));
		$row = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($tablename, $record->getUid());
		if ($row) {
			GeneralUtility::loadTCA($tablename);
			#$fields = implode(',',array_keys($GLOBALS['TCA'][$tablename]['columns']));
			$fields = implode(',',array_keys($row));


			#$fields = $GLOBALS['TCA'][$tablename]['types'][0];
			$title = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordTitle($tablename,$row);
			$conf=array(
					'beforeLastTag'=>1,
					'iconTitle' => $title
			);

			return $GLOBALS['TSFE']->cObj->editIcons('',$tablename.':'.$fields,$conf,$tablename.':'.$row['uid'],$row,'&viewUrl='.rawurlencode(\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REQUEST_URI')));
		}
	}

}

?>