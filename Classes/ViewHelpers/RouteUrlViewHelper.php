<?php


namespace Bitmotion\BmLocations\ViewHelpers;

use \TYPO3\CMS\Core\Utility\GeneralUtility;

class RouteUrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {


	/**
	 * Creates url for google maps
	 *
	 * @param \Bitmotion\BmLocations\Domain\Model\Location $location
 	 * @return string
	 */
	public function render($location)
	{
		$strAddress = '';
		$strAddress .= $location->getLocationForRouting() . "\n";
		#$strAddress .= '(' . $location->getCompetence()->getName() . ")";
		$strAddress .= '(' . $location->getCountryName() . ")";
		$strAddress = GeneralUtility::trimExplode("\n", $strAddress, true);
		$strAddress = implode(",", $strAddress);

		// this might not be the right code
		$languageKey = $GLOBALS['TSFE']->config['config']['locale_all'];
		$EncodingType = $GLOBALS['TSFE']->config['config']['renderCharset'];


		$strParameterArray = array('ie' => $EncodingType, 'hl' => $languageKey, 'iwstate1' => 'dir:to', 'daddr' => $strAddress,);

		$strUrl = 'https://maps.google.com/maps?' . $this->ImplodeParameterArray($strParameterArray, null, false, true);

		return $strUrl;
	}


	/**
	 * Implodes a multidimensional-array into GET-parameters (eg. &param[key][key2]=value2&param[key][key3]=value3)
	 *
	 * This can be used statically. (But can't be declared static because of $this->_strPrefix :-/ )
	 *
	 * @param	array		The (multidim) array to implode
	 * @param	string|false|null	Name prefix for entries. Set to false if you wish none.
	 * @param	boolean		If set, parameters which were blank strings would be removed.
	 * @param	boolean		If set, the param name itself (for example "param[key][key2]") would be rawurlencoded as well.
	 * @return	string		Imploded result, fx. &param[key][key2]=value2&param[key][key3]=value3
	 * @todo replace with internal PHP function http_build_query?
	 */
	protected function ImplodeParameterArray($theArray, $name=null, $skipBlank=true, $rawurlencodeParamName=false)
	{
		// if we're not used in static context and use the current prefix
		if (is_null($name) AND $this instanceof \Bitmotion\BmLocations\Utility\NextFunctions\Url) {
			$name = $this->_strPrefix;
		}
		$str = '';
		if (is_array($theArray)) {
			foreach ($theArray as $Akey => $AVal) {
				$thisKeyName = $name ? $name . '[' . $Akey . ']' : $Akey;
				if (is_array($AVal)) {
					$str .= self::ImplodeParameterArray($AVal, $thisKeyName, $skipBlank, $rawurlencodeParamName);
				} else {
					if (!$skipBlank || strcmp($AVal, '')) {
						$str.='&' . ($rawurlencodeParamName ? rawurlencode($thisKeyName) : $thisKeyName) .
						'=' . rawurlencode($AVal);
					}
				}
			}
		}
		return $str;
	}

}

?>
