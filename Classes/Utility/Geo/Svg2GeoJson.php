<?php


namespace Bitmotion\BmLocations\Utility\Geo;


class Point {
	public $X = 0.0;
	public $Y = 0.0;

	public function toArray()
	{
		return array($this->X, $this->Y);
	}
}


/**
 * Class for parsing svg files for json output.
 *
 */
class Svg2GeoJson
{

	/**
	 * Takes a SVG file and converts it into an array for geo json usage
	 *
	 *
	 * @param   string   svg file path
	 * @param   string   xpath string which elements should be parsed
	 * @return  array
	 */
	public function convert($svgFilePath, $xpath)
	{
		// template objects
		$jsonRegionsFeatureCollection = '{
				"type": "FeatureCollection",
				"features": []
			}';
		#$regionsFeatureCollectionTemplate = json_decode($jsonRegionsFeatureCollection);

		// we use always MultiPolygon even if there's only one
		$jsonPolygonFeature = '{
				"type": "Feature",
				"properties": { },
				"geometry": {
					"type": "MultiPolygon",
					"coordinates": []
				}
			}';
		#$polygonFeatureTemplate = json_decode($jsonPolygonFeature);


		$countriesRegion = array();

		$svgContent = file_get_contents($svgFilePath);
		// Illustrator does it this way, which doesn not work with the php xml parser
		$svgContent = str_replace('xmlns="&ns_svg;"', 'xmlns="http://www.w3.org/2000/svg"', $svgContent);
		$xml = simplexml_load_string($svgContent);
		$xml->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
		$namespaces = $xml->getNamespaces(true);
		foreach ($namespaces as $prefix => $ns) {
			$xml->registerXPathNamespace($prefix, $ns);
		}

		$paths = $xml->xpath($xpath);

		if ($paths) {

			foreach ($paths as $path) {
				list($countryIso2, $h_or_s, $regionNr) = explode('-', (string)$path->attributes()->id);

				if (!isset($countriesRegion[$countryIso2])) {
					// clone doesn't work!
					#$countriesRegion[$countryIso2] = clone ($regionsFeatureCollectionTemplate);
					$countriesRegion[$countryIso2] = json_decode($jsonRegionsFeatureCollection);
				}

				$coordinates = (string)$path->attributes()->d;

				// clone doesn't work!
				#$polygonFeature = clone($polygonFeatureTemplate);
				$polygonFeature = json_decode($jsonPolygonFeature);
				$polygonFeature->properties->region = strtolower($countryIso2).'-'.intval($regionNr);
				$polygonFeature->properties->iso_a2 = $countryIso2;
				$polygonFeature->properties->name = intval($regionNr);
				$coordinates = self::parseSvgPaths($coordinates);
				$polygonFeature->geometry->coordinates = $coordinates;

				$countriesRegion[$countryIso2]->features[] = $polygonFeature;
			}
		}

		return $countriesRegion;
	}


	protected function parseSvgPaths($coordinateString)
	{
		$translation = new Point();
		$translation->X = -180.0;
		$translation->Y = 90.0;

		$scale = new Point();
		$scale->X = 1.0;
		$scale->Y = -1.0;

		$initPoint = new Point(); //Initial point of the subpath
		$endPoint = new Point(); //End point of the (previous) command

		$currentPoint = new Point(); //Current point of the command


		$path = array();
		$paths = array();


		preg_match_all('/([a-z])([-.,\d ]*)/si', $coordinateString, $pieces, PREG_SET_ORDER);

		for ($i=0; $i<count($pieces); $i++) {
			preg_match_all('/([+-]?(?:\d+\.?\d*|\.\d+))/i', $pieces[$i][2], $piecesParts);
			$pieces[$i] = array_merge(array($pieces[$i][1]), $piecesParts[0]);
		}


		// see http://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands

		foreach ($pieces as $elements) {

			$command = array_shift($elements);

			$isRelative = false;
			if(preg_match("/[A-Z]/", $command)===0) {
				$isRelative = true;
			}

			switch ($command) {
				case 'M':
				case 'm': //Open subpath

					if ($path) {
						$paths[] = array($path);
					}
					$path = array();
					$initPoint = new Point();

					$initPoint->X = ($isRelative ? $currentPoint->X : $translation->X) + ((float)$elements[0] * $scale->X);
					$initPoint->Y = ($isRelative ? $currentPoint->Y : $translation->Y) + ((float)$elements[1] * $scale->Y);

					$endPoint = $initPoint;
					$currentPoint = $initPoint;
					break;

				case 'Z':
				case 'z':
					// we generate polygons so this is not needed
 					$currentPoint = $initPoint; //Init point becomes the current point
 					//$path[] = $currentPoint;
 					$paths[] = array($path);
					$path = array();
					break;

				case 'C':
				case 'c':
					$points = array();
					$points[] = $endPoint->toArray();
					$n = 0;
					for ($i = 0; $i < count($elements); $i += 2) {
						if (++$n >= 3) { //Not a control point
							$point = new Point();
							$point->X = ($isRelative ? $currentPoint->X : $translation->X) + ((float)$elements[$i] * $scale->X);
							$point->Y = ($isRelative ? $currentPoint->Y : $translation->Y) + ((float)$elements[$i + 1] * $scale->Y);

							$points[] = $point->toArray();
							$currentPoint = $point;
							$endPoint = $point;
							$n = 0;
						}
					}
					$path = array_merge($path, $points);
					break;


				case 'S':
				case 's':
					$points = array();
					$points[] = $endPoint->toArray();
					$n = 0;
					for ($i = 0; $i < count($elements); $i += 2) {
						if (++$n >= 2) { //Not a control point
							$point = new Point();
							$point->X = ($isRelative ? $currentPoint->X : $translation->X) + ((float)$elements[$i] * $scale->X);
							$point->Y = ($isRelative ? $currentPoint->Y : $translation->Y) + ((float)$elements[$i + 1] * $scale->Y);

							$points[] = $point->toArray();
							$currentPoint = $point;
							$endPoint = $point;
							$n = 0;
						}
					}
					$path = array_merge($path, $points);
					break;

				case 'L':
				case 'l':
					$points = array();
					$points[] = $endPoint->toArray();
					for ($i = 0; $i < count($elements); $i += 2) {
						$point = new Point();
						$point->X = ($isRelative ? $currentPoint->X : $translation->X) + ((float)$elements[$i] * $scale->X);
						$point->Y = ($isRelative ? $currentPoint->Y : $translation->Y) + ((float)$elements[$i + 1] * $scale->Y);

						$points[] = $point->toArray();
						$currentPoint = $point;
					}
					$endPoint = $currentPoint;
					$path = array_merge($path, $points);
					break;

				case 'H':
				case 'h':
					$Y = $endPoint->Y;
					$points = array();
					for ($i = 0; $i < count($elements); $i++) {
						$point = new Point();
						$point->X = ($isRelative ? $currentPoint->X : $translation->X) + ((float)$elements[$i] * $scale->X);
						$point->Y = $Y;

						$points[] = $point->toArray();
						$currentPoint = $point;
					}
					$endPoint = $currentPoint;
					$path = array_merge($path, $points);
					break;

				case 'V':
				case 'v':
					$X = $endPoint->X;
					$points = array();
					for ($i = 0; $i < count($elements); $i++) {
						$point = new Point();
						$point->X = $X;
						$point->Y = ($isRelative ? $currentPoint->Y : $translation->Y) + ((float)$elements[$i] * $scale->Y);

						$points[] = $point->toArray();
						$currentPoint = $point;
					}
					$endPoint = $currentPoint;
					$path = array_merge($path, $points);
					break;
				default:
// 					var_dump('co: '.$coordinate);
// 					var_dump($elements);
// 					var_dump($command);
					break;
			}

		}
		if ($path) {
			$paths[] = array($path);
		}

		return $paths;

	}

}
