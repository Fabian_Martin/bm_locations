<?php
namespace Bitmotion\BmLocations\Utility\Geo;

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Backend\Form\NodeFactory;
use TYPO3\CMS\Backend\Form\NodeInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;


/**
 * Provide TCEforms functions for usage in own extension.
 *
 */
class GoogleMap extends AbstractFormElement implements NodeInterface {

    private $PA;
    private $fobj;

    function __construct(NodeFactory $nodeFactory, array $data)
    {
        $this->PA = $data;
        $this->fobj = $nodeFactory;
        parent::__construct($nodeFactory, $data);
    }

    /**
     * @return array
     */
    function render(){

        $resultArray = $this->initializeResultArray();
        $fieldWizardResult = $this->renderFieldWizard();
        $resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldWizardResult, false);
        $longitude = $this->PA['databaseRow']['longitude'];
        $latitude = $this->PA['databaseRow']['latitude'];

        if((empty($longitude) || empty($latitude)) && $this->PA['databaseRow']['auto_coordinates'] === 1){
            $map = $this->displayMap($longitude, $latitude);
            $resultArray['html'] = $map;

        }elseif ($longitude && $latitude){
            $map = $this->displayMap($longitude, $latitude);
            $resultArray['html'] = $map;
        }
        return $resultArray;
    }


    /**
     * @param $longitude
     * @param $latitude
     * @return null|string
     */
    private function displayMap($longitude, $latitude){
        $address = $this->getParamsForAutoCoordinates();

        $map = [];
        $map[] = '<style>#map {width: 90%; height: 250px;}</style>';
        $map[] = '<h3>Map</h3>';
        $map[] = '<div id="map"></div>';
        $map[] = '<script>
            var latitude = "'.$latitude.'";
            var longitude = "'.$longitude.'";
            var address = "'.$address.'";
            
            function createMap (latitude, longitude) {
                var LatLng = new google.maps.LatLng(latitude, longitude);
                var map = new google.maps.Map(document.getElementById(\'map\'), {
                    zoom: 4,
                    center: LatLng
                });
                var marker = new google.maps.Marker({
                    position: LatLng,
                    map: map,
                    draggable: true
                });
                google.maps.event.addListener(marker, \'drag\', function (evt) {
                    document[TBE_EDITOR.formname]["data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][longitude]"].value = marker.getPosition().lng();
                    document.querySelectorAll(\'[data-formengine-input-name="data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][longitude]"]\')[0].value = marker.getPosition().lng();
                    document[TBE_EDITOR.formname]["data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][latitude]"].value = marker.getPosition().lat();
                    document.querySelectorAll(\'[data-formengine-input-name="data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][latitude]"]\')[0].value = marker.getPosition().lat();
                });
            }
            
            function initMap() {
                if(!latitude || !longitude) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ "address": address}, function(results, status) {
                        if (status == \'OK\') {
                            latitude = results[0].geometry.location.lat();
                            longitude = results[0].geometry.location.lng();
                            document[TBE_EDITOR.formname]["data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][longitude]"].value = longitude;
                            document.querySelectorAll(\'[data-formengine-input-name="data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][longitude]"]\')[0].value = longitude;
                            document[TBE_EDITOR.formname]["data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][latitude]"].value = latitude;
                            document.querySelectorAll(\'[data-formengine-input-name="data[tx_bmlocations_domain_model_location]['. $this->PA['databaseRow']['uid'] .'][latitude]"]\')[0].value = latitude;
                            createMap(latitude, longitude);
                        }else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });    
                } else {
                    createMap(latitude, longitude);
                }
            }
        </script>';
        $map[] = '<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkmCe4hZjiYbkom-dlDgXU3USpG9Q-QZM&callback=initMap&sensor=false"></script>';
        $map = implode($map);
        return $map;
    }

    private function getParamsForAutoCoordinates(){
        $paramsArray = Array(
            'adress' => '',
            'city' => '',
            'zip' => '',
        );
        $paramsString = '';

        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQuerybuilderForTable('static_countries');

        $countryName = $queryBuilder->select('cn_official_name_local')
            ->from('static_countries')
            ->where(
                $queryBuilder->expr()
                    ->eq(
                        'uid', (int)$this->PA['databaseRow']['country'][0]
                    )
            )->execute()->fetch();

        foreach ($paramsArray as $parameter => $value){
            if(!empty($this->PA['databaseRow'][$parameter])){
                $paramsString .= $this->PA['databaseRow'][$parameter].',';
            }
        }
        if($countryName){
            $paramsString .= $countryName['cn_official_name_local'];
        }else {
            $paramsString = substr($paramsString, 0, -1);
        }

        return $paramsString;
    }
}

