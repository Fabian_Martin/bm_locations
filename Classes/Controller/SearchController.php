<?php

namespace Bitmotion\BmLocations\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Rene Fritz <r.fritz@bitmotion.de>, Bitmotion
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package bd_locations
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SearchController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * locationRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\LocationRepository
	 * @inject
	 */
	protected $locationRepository;

	/**
	 * competenceRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\CompetenceRepository
	 * @inject
	 */
	protected $competenceRepository;

	/**
	 * divisionRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\DivisionRepository
	 * @inject
	 */
	protected $divisionRepository;




	/**
	 * action list
	 *
	 * @param string $selectedCountry
	 * @param string $inputZip
	 * @return void
	 */
	public function listAction($selectedCountry=null, $inputZip=null)
	{
		// this is sooo boooring
		$this->view->assign('selectedCountry', $selectedCountry);
		$this->view->assign('inputZip', $inputZip);
		$this->view->assign('enableZipSearch', $selectedCountry=='DE');

		$enableZipSearch = $selectedCountry=='DE';

		$division = $this->divisionRepository->findByUid($this->settings['division']);

		// this was easy :-)
		$countries = $this->competenceRepository->findByDivision($division);
		$countriesArray = array();
		$countriesSortedArray = array();
		$countriesNamesArray = array();
		foreach ($countries as $country) {
			$countriesArray[$country->getUid()] = $country;
			$countriesNamesArray[$country->getUid()] = $country->GetCountryName();
		}
		if (class_exists('Collator')) {
			$collator = new \Collator(setlocale(LC_COLLATE, 0));
			$collator->asort($countriesNamesArray);
		} else {
			asort($countriesNamesArray);
		}
		foreach ($countriesNamesArray as $key => $value) {
			$countriesSortedArray[] = $countriesArray[$key];
		}
		unset($countries);
		unset($countriesNamesArray);


		/* this would be the easy way but hey I don't want to write a view helper just for this - urrghh
		$countries = $this->competenceRepository->findByDivision($division);
		$countriesNamesArray = array();
		foreach ($countries as $country) {
			$countriesNamesArray[$country->getIsoCode()] = $country->GetCountryName();
		}
		$collator = new Collator(setlocale(LC_COLLATE, 0));
		$collator->asort($countriesNamesArray);
		*/


		if ($selectedCountry) {
			if ($enableZipSearch AND intval($inputZip)) {
				$locations = $this->locationRepository->findByCountryAndDivisionAndZip($selectedCountry, $this->settings['division'], $inputZip);
			} else {
				$locations = $this->locationRepository->findByCountryAndDivision($selectedCountry, $this->settings['division']);
			}
		}

		$this->view->assign('countries', $countriesSortedArray);
		$this->view->assign('locations', $locations);
		// ->count() does NOT work here - bug in extbase I guess
		if ($locations) {
			$this->view->assign('locationsCount', count($locations->toArray()));
		} else {
			$this->view->assign('locationsCount', 0);
		}
	}

}
?>