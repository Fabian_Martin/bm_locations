<?php

namespace Bitmotion\BmLocations\Controller;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 *
 *
 * @package bm_locations
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class MapController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * competenceRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\CompetenceRepository
     * @inject
     */
    protected $competenceRepository = null;


    /**
     * locationRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository;


    /**
     * divisionRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\DivisionRepository
     * @inject
     */
    protected $divisionRepository;

    /**
     * consultantTypeRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\ConsultantTypeRepository
     * @inject
     */
    protected $consultantTypeRepository;

    /**
     * regionRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\RegionRepository
     * @inject
     */
    protected $regionRepository;

    /**
     * action list
     *
     * @param string $cn Country
     * @param string $loc Location
     * @param string $rg Map region id
     * @param string $ct Consultant type
     * @param string $dv division
     * @param string $co coordinates
     *
     * @return void
     */
    public function listAction($cn = null, $loc = null, $rg = null, $ct = null, $dv = null, $co = null)
    {
        $locations = $this->locationRepository->findMatchingForMap(
            $this->settings['division'],
            $this->settings['consultant_type'],
            $this->settings['countries']
        );

        $countryList = array();
        $markerLocations = array();

        foreach ($locations as $location) {

            /* @var $location \Bitmotion\BmLocations\Domain\Model\Location */
            if ($location['longitude'] != 0.0 && $location['latitude'] != 0.0) {
                $markerLocations[] = $location;
            }

            /*
             * Construct an null-values array using the country ISO codes as keys.
             * The values will be assigned below after filtering duplicates for performance reasons.
             */
            if ($location['country']) {
                $countryList[$location['country']] = $location['country'];
            }
        }

        //DebuggerUtility::var_dump($markerLocations, 'markerlocations');
        $markerLocations = $this->filterDoubleLocations($markerLocations);

        /*
         * Populate country list with full names resolved by TYPO3 based on their ISO codes set above.
         */
        /*
        foreach ($countryList as $isoCode => $dummy) {
            $countryList[$isoCode] = \Bitmotion\BmLocations\Utility\StaticInfoTablesHelper::getTitleFromIsoCode('static_countries', $isoCode);
        }*/

        $limitCountriesIsoList = $this->settings[ 'countries' ];

        if ($limitCountriesIsoList) {
            $countries = $this->competenceRepository->findByCountryAndDivision($limitCountriesIsoList,
                $this->settings[ 'division' ]);
        } elseif ($this->settings[ 'division' ]) {
            $countries = $this->competenceRepository->findByDivision($this->settings[ 'division' ]);
        } else {
            $countries = $this->competenceRepository->findAll();
        }

        $countryList = array();
        $regionList = array();
        $regionMapList = array();

        foreach ($countries as $country) {
            /* @var $country \Bitmotion\BmLocations\Domain\Model\Competence */
            $countryList[] = $country->getCountry();
        }

        if (class_exists('Collator')) {
            // workaround for broken utf8
            asort($countryList);
            // now try to sort the right way
            $collator = new \Collator(setlocale(LC_COLLATE, 0));
            $collator->asort($countryList);
        } else {
            asort($countryList);
        }

        $consultantTypes = array();


        foreach ($markerLocations as $location) {
            /* @var $location \Bitmotion\BmLocations\Domain\Model\Location */
            if ('' == $location['latitude']) {
                continue;
            }
            if ('' == $location['longitude']) {
                continue;
            }
            if (0.0 == (float)$location['latitude'] AND 0.0 == (float)$location['longitude']) {
                continue;
            }

            if ($consultantType = $location['consultanttype']) {
                $uid = $location['consultanttype'];
                $consultantTypes[ $uid ] = $consultantType;
            }
        }

        // sort legend
        $consultantTypeUidList = explode(',', $this->settings['consultanttype']);
        $legend = array();
        foreach ($consultantTypeUidList as $uid) {
            if ($consultantTypes[ $uid ]) {
                $legend[ $uid ] = $consultantTypes[ $uid ];
            }
        }

        $countryListSelect = array(' ') + $countryList;

        // get country code when a single country is configured
        /*reset($countryList);
        $singleCountry = count($countryList) == 1 ? key($countryList) : false;

        if ($singleCountry AND !$rg) {
            $region = $this->regionRepository->findOneByCountryAndDivision($singleCountry,
                $this->settings[ 'division' ]);
            if (!$region) {
                $this->addressListAction($singleCountry, $loc, $rg, $ct, $this->settings[ 'division' ], $co);
            }
        } else {
            $this->addressListAction($cn, $loc, $rg, $ct, $dv, $co);
        }*/

        // cleanup array
        $this->settings[ 'consultantTypeOnWorldmap' ] = explode(',', $this->settings[ 'consultantTypeOnWorldmap' ]);
        $this->settings[ 'consultantTypeOnWorldmap' ] = array_flip($this->settings[ 'consultantTypeOnWorldmap' ]);
        array_walk($this->settings[ 'consultantTypeOnWorldmap' ], function (&$value, $key) {
            $value = $key;
        });

        $this->view->assign('settings', $this->settings);
        $this->view->assign('markerLocations', $markerLocations);
        $this->view->assign('countryList', $countryList);
        //$this->view->assign('singleCountry', $singleCountry);
        $this->view->assign('countryListSelect', $countryListSelect);
        $this->view->assign('regionList', $regionList);
        $this->view->assign('regionMapList', $regionMapList);
        $this->view->assign('legend', $legend);
        $this->view->assign('consultantTypeList', $this->settings[ 'consultanttype' ]);
        $this->view->assign('requestUri', $_SERVER[ 'REQUEST_URI' ]);
    }


    /**
     * action list
     *
     * @param string $cn Country
     * @param string $loc Location
     * @param string $rg Map region id
     * @param string $ct Consultant type
     * @param string $dv division
     * @param string $co coordinates
     * @return void
     */
    public function addressListAction($cn = null, $loc = null, $rg = null, $ct = null, $dv = null, $co = null)
    {
        $debug = array();
        $debug[ '$cn' ] = $cn;
        $debug[ '$loc' ] = $loc;
        $debug[ '$rg' ] = $rg;
        $debug[ '$ct' ] = $ct;
        $debug[ '$dv' ] = $dv;
        $debug[ '$co' ] = $co;

        if ($ct === null && isset($this->settings['consultanttype']) && !empty($this->settings['consultanttype'])) {
            $ct = $this->settings['consultanttype'];
        }

        /*
         * Either division provided by parameter or, if not a valid integer, what is defined in TypoScript.
         */
        $division = $dv ? intval($dv) : $this->settings[ 'division' ];

        $this->view->assign('selectedCountry', $cn);

        if ($division) {
            $countries = $this->competenceRepository->findByDivision($division);
        } else {
            $countries = $this->competenceRepository->findAll();
        }

        $consultanttype = $this->settings[ 'consultanttype' ] ? $this->settings[ 'consultanttype' ] : $ct;
        $consultanttype = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $consultanttype, true);
        $ct = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $ct, true);
        $consultanttype = array_intersect($consultanttype, $ct);
        $consultanttype = implode(',', $consultanttype);


        $debug['$consultanttype'] = $consultanttype;

        if ($co) {

            $debug[ 'use $co' ] = '';

            $locations = $this->locationRepository->findByDistance($cn, $division, $consultanttype, $co);

        } elseif ($rg) {
            $debug[ 'use $rg' ] = '';

            // TODO $cn is set here too - maybe useful?

            $region = $this->regionRepository->findOneByMapRegionIdAndDivision($rg, $division);

            if ($region) {
                $locations = $this->locationRepository->findByRegionAndConsultantType($region, $consultanttype);
                $locations->rewind();
            }

        } elseif ($cn) {

            $debug[ 'use $cn' ] = '';

            $locations = $this->locationRepository->findMatching($division, $consultanttype, $cn);

        } elseif ($loc) {
            $debug[ 'use $loc' ] = '';

            $loc = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $loc, true);
            $locations = $this->locationRepository->findAllByUid($loc);
        }

        //DebuggerUtility::var_dump($co);
        //DebuggerUtility::var_dump($rg);
        //DebuggerUtility::var_dump($cn);
        //DebuggerUtility::var_dump($loc);


        $locations = $this->filterDoubleLocations($locations);
        $locationsCount = count($locations);

        $debug['$locationsCount'] = $locationsCount;

        $this->view->assign('settings', $this->settings);
        $this->view->assign('countries', $countries);
        $this->view->assign('locations', $locations);
        $this->view->assign('locationsCount', $locationsCount);

        foreach ($locations as $value) {
            if ($value->getConsultant()) {
                $debug[ $value->getUid() ] = $value->getConsultant()->getName();
            } else {
                $debug[ $value->getUid() ] = 'missing consultant';
            }
        }

        $debug[ 'settings' ] = '';
        foreach ($this->settings as $key => $value) {
            $debug[ $key ] = $value;
        }

        if ($debug) {
            foreach ($debug as $key => $value) {
                $content .= $key . ': ' . $value . "\n";
            }
            $debug = "\n\nDebugausgabe\n" . $content;
            // var_dump($debug);
        }
        $this->view->assign('debug', $debug);
    }


    /**
     * action list
     *
     * @param string $cn
     * @param string $zip
     * @param string $ct Consultant type
     * @param string $dv division
     * @return void
     */
    public function addressListByZipAction($cn, $zip, $ct = null, $dv = null)
    {
        $debug = array();
        $debug[ '$cn' ] = $cn;
        $debug[ '$zip' ] = $zip;
        $debug[ '$ct' ] = $ct;
        $debug[ '$dv' ] = $dv;


        $division = $dv ? intval($dv) : $this->settings[ 'division' ];

        $debug[ '$division' ] = $division;

        if ($ct) {
            $ct = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $ct, true);
        }

        $consultanttype = $this->settings[ 'consultanttype' ] ? $this->settings[ 'consultanttype' ] : $ct;
        $consultanttype = array_intersect($consultanttype, $ct);
        $consultanttype = implode(',', $consultanttype);


        $debug[ '$consultanttype' ] = $consultanttype;


        if (intval($zip)) {
            $locations = $this->locationRepository->findByCountryAndDivisionAndZip($cn, $division, $zip);

            $locations = $this->filterDoubleLocations($locations);
        }

        $this->view->assign('locations', $locations);
        // ->count() does NOT work here - bug in extbase I guess
        if ($locations) {
            $this->view->assign('locationsCount', count($locations));
        } else {
            $this->view->assign('locationsCount', 0);
        }


        $debug[ 'settings' ] = '';
        foreach ($this->settings as $key => $value) {
            $debug[ $key ] = $value;
        }

        if ($debug) {
            foreach ($debug as $key => $value) {
                $content .= $key . ': ' . $value . "\n";
            }
            $debug = "\n\nDebugausgabe\n" . $content;
        }
        $this->view->assign('debug', $debug);
    }


    /**
     * This prevents locations to be shown twice in a list
     * This could happen when The map should show all divisions
     *
     * @param array $locations
     * @return mixed:\Bitmotion\BmLocations\Domain\Model\Location
     */
    protected function filterDoubleLocations($locations)
    {
        $filteredLocationUids = array();
        $filteredLocations = array();

        foreach ($locations as $location) {
            // TODO use constant
            if ($location['type'] == 0) {
                $baseUid = $location['uid'];
            } else {
                // this is a reference
                if (!is_object($location['baseAddress'])) {
                    // this should not happen
                    continue;
                }
                $baseUid = $location->getBaseAddress()->getUid();
            }

            if (!in_array($baseUid, $filteredLocationUids)) {
                $filteredLocationUids[] = $baseUid;
                $filteredLocations[] = $location;
            }
        }

        return $filteredLocations;
    }
}

?>
