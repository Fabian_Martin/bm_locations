<?php
namespace Bitmotion\BmLocations\Controller;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * LocationController
 */
class LocationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * locationRepository
     *
     * @var \Bitmotion\BmLocations\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $locations = $this->locationRepository->findAll();
        $this->view->assign('locations', $locations);
    }

    /**
     * action show
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Location $location
     * @return void
     */
    public function showAction(\Bitmotion\BmLocations\Domain\Model\Location $location)
    {
        $this->view->assign('location', $location);
    }
}
