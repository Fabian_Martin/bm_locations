<?php
namespace Bitmotion\BmLocations\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Rene Fritz <r.fritz@bitmotion.de>, Bitmotion
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package bd_locations
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ExportController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * countryRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\CompetenceRepository
	 * @inject
	 */
	protected $countryRepository;


	/**
	 * divisionRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\DivisionRepository
	 * @inject
	 */
	protected $divisionRepository;


	/**
	 * competenceRepository
	 *
	 * @var \Bitmotion\BmLocations\Domain\Repository\CompetenceRepository
	 * @inject
	 */
	protected $competenceRepository;




	/**
	 * initialize the controller
	 *
	 * @return void
	 */
	protected function initializeAction() {
		parent::initializeAction();
		//fallback to current pid if no storagePid is defined
		$configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);

		if(empty($configuration['persistence']['storagePid'])){
			#fixme
			$currentPid['persistence']['storagePid'] = ($GLOBALS["TSFE"] ? $GLOBALS["TSFE"]->id : intval($_GET['id']));
			$this->configurationManager->setConfiguration(array_merge($configuration, $currentPid));
		}
	}



	/**
	 * Shows the import jobs selection .
	 *
	 * @return string The rendered view
	 */
	public function indexAction()
	{
		$divisions = $this->divisionRepository->findAll();

		$this->view->assign('divisions', $divisions);
	}



	/**
	 * action show
	 *
	 * @param \Bitmotion\BmLocations\Domain\Model\Division $division
	 * @return void
	 */
	public function pdfAction($division=null)
	{
		$division = $division ? $division : $this->divisionRepository->findOneByUid(($this->settings['division']?$this->settings['division']:1));
		$pdf = new \Bitmotion\BmLocations\Pdf\ContactsList;
		$pdf->setCompetenceRepository($this->competenceRepository);
		$pdf->setDivision($division);
		//$pdf->setCountry(62);
		$pdf->Output('Verkaufsberater-'.$division->getName().'.pdf', 'D');
		exit;
	}

}
?>