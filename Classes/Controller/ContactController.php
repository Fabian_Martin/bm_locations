<?php

namespace Bitmotion\BmLocations\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Florian Wessels <typor-ext@bitmotion.de>, Bitmotion
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
use Bitmotion\BmLocations\Domain\Model\Location;
use Bitmotion\BmLocations\Utility\VCardUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class ContactController extends ActionController
{
    /**
     * @var \Bitmotion\BmLocations\Domain\Repository\LocationRepository
     * @inject
     */
    protected $locationRepository;

    /**
     * @return void
     */
    public function vCardAction()
    {
        $location = $this->locationRepository->findByUid($this->request->getArgument('location'));

        if ($location instanceof Location) {
            $vCard = VCardUtility::createVCard($location);

            $filename = $vCard['name'] . '.vcf';
            $fileOpened = fopen('php://output', 'w+');

            fwrite($fileOpened, $vCard['vCard']);
            fclose($fileOpened);

            $this->response->setHeader('Pragma', 'no-cache');
            $this->response->setHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
            $this->response->setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
            $this->response->setHeader('Content-Description', 'File Transfer');
            $this->response->setHeader('Content-Transfer-Encoding', 'binary');
            $this->response->setHeader('Content-Type', 'text/vcard; charset=utf-8');
            $this->response->setHeader('Content-Disposition', 'attachment; filename="' . $filename . '"');
            $this->response->sendHeaders();

            flush();
        }

        exit();
    }
}