<?php
namespace Bitmotion\BmLocations\Domain\Repository;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Competences
 */
class CompetenceRepository extends Repository
{
    /**
     * defaultOrderings
     *
     * @var array
     */
    protected $defaultOrderings = array("name"=> \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING);

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     */
    public function createQuery() {
        $query = parent:: createQuery();
        $query->getQuerySettings()->setLanguageMode('content_fallback');
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        return $query;
    }


    /**
     * @param $country
     * @param $division
     * @return array
     */
    public function findByCountryAndDivision($country, $division)
    {
        $countries = explode(',', $country);

        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_competence');
        $query = $queryBuilder->select('*')->from('tx_bmlocations_domain_model_competence')->where(
            $queryBuilder->expr()->in('country', $countries)
        )->andWhere(
            $queryBuilder->expr()->eq('division', $division)
        );
/*
        $query = $this->createQuery();

        $constraints = array ();
        if ($division) {
            $constraints [] = $query->equals ('division', $division );
        }
        $constraints [] = $query->in('iso_code', $isoCodes);

        $query->matching(
            $query->logicalAnd($constraints)
        );*/

        $competence = $query->execute();
        return $competence;
    }

    /**
     * @param $isoCode
     * @param $division
     * @param $consultantType
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCountryAndDivisionAndConsultantType($isoCode, $division, $consultantType)
    {
        $isoCodes = explode(',', $isoCode);

        $query = $this->createQuery();

        $constraints = array ();
        if ($division) {
            $constraints [] = $query->equals ('division', $division );
        }
        $constraints [] = $query->in('iso_code', $isoCodes);
        $constraints [] = $query->in('locations.consultanttype', $consultantType);

        $query->matching(
            $query->logicalAnd($constraints)
        );
        $competence = $query->execute();
        return $competence;
    }
}
