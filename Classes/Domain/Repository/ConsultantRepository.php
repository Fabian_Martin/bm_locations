<?php
namespace Bitmotion\BmLocations\Domain\Repository;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Consultants
 */
class ConsultantRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
