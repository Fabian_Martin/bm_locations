<?php
namespace Bitmotion\BmLocations\Domain\Repository;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Locations
 */
class LocationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Initializes the repository.
     *
     * @return void
     */
    public function initializeObject() {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        $querySettings->setLanguageMode('content_fallback');
        $querySettings->setRespectStoragePage(FALSE);
        #$querySettings->setRespectSysLanguage(FALSE);

        $this->setDefaultQuerySettings($querySettings);
    }


    /**
     * @param $uid
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllByUid($uid)
    {

        if (! is_array ( $uid )) {
            $uid = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode ( ',', $uid, true );
        }
        foreach ( $uid as $key => $value ) {
            $uid [$key] = intval ( $value );
        }
        $uidList = implode ( ',', $uid );

        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_location');
        $query = $queryBuilder->select('*')->from('tx_bmlocations_domain_model_location')->where(
            $queryBuilder->expr()->in('uid', $uidList)
        )->orderBy($uidList);

/*
        $query = $this->createQuery ();
        $query->statement ( "
				SELECT tx_bdlocations_domain_model_location.*
				FROM tx_bdlocations_domain_model_location
				WHERE tx_bdlocations_domain_model_location.uid IN($uidList)
				ORDER BY FIELD(tx_bdlocations_domain_model_location.uid, $uidList)" );
*/
        return $query->execute()->fetchAll();
    }

    /**
     * @param $division
     * @param null $consultantType
     * @param null $country
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findMatchingForMap($division, $consultantType = NULL, $country = NULL)
    {
        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_location');

        $query = $queryBuilder->select('*')->from('tx_bmlocations_domain_model_location');

        $constraints = array ();
        if ($division) {
            //$query->andWhere($queryBuilder->expr()->eq('competence.division', $division, \PDO::PARAM_INT));
            $queryBuilder->join(
                'tx_bmlocations_domain_model_location',
                'tx_bmlocations_domain_model_competence',
                'competence',
                $query->expr()->eq('competence.division', $division, \PDO::PARAM_INT));
            /*
            $constraints['and'][] = array('division', 'eq', $division);
            $constraints [] = $query->equals ( 'competence.division', $division );
            */
        }

        $query->andWhere($queryBuilder->expr()->eq('hide_location', 0, \PDO::PARAM_INT));
        //$constraints [] = $query->equals ( 'hide_location', 0 );

        if ($country and ($countries = explode ( ',', $country ))) {
            $queryBuilder->join(
                'tx_bmlocations_domain_model_location',
                'tx_bmlocations_domain_model_competence',
                'competence',
                $query->expr()->in('competence.country', 0, \PDO::PARAM_INT));
            //$query->andWhere($queryBuilder->expr()->in('competence.country', 0, \PDO::PARAM_INT));
            //$constraints [] = $query->in ( 'competence.iso_code', $countries );
        }

        if ($consultantType and ($consultantTypes = explode ( ',', $consultantType ))) {
            $query->andWhere($queryBuilder->expr()->in('consultanttype', $consultantTypes, \PDO::PARAM_INT));
            //$constraints [] = $query->in ( 'consultant_type', $consultantTypes );
        }

        //$query->matching($query->logicalAnd ($constraints) );
        $locations = $query->execute()->fetchAll();

        return $locations;
    }

    /**
     * @param $division
     * @param null $consultantType
     * @param null $isoCode
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findMatching($division, $consultantType = NULL, $isoCode = NULL)
    {
        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_location');

        $query = $queryBuilder->select('*')->from('tx_bmlocations_domain_model_location');

        $constraints = array ();
        if ($division) {
            $query->andWhere($queryBuilder->expr()->eq('competence.division', $division, \PDO::PARAM_INT));
            /*
            $constraints [] = $query->equals ( 'competence.division', $division );
            */
        }

        if ($isoCode and ($isoCodes = explode ( ',', $isoCode ))) {
            $query->andWhere($queryBuilder->expr()->in('competence.country', 0, \PDO::PARAM_INT));
            /*
            $constraints [] = $query->in ( 'competence.iso_code', $isoCodes );
            */
        }

        if ($consultantType and ($consultantTypes = explode ( ',', $consultantType ))) {
            $query->andWhere($queryBuilder->expr()->in('consultanttype', $consultantTypes, \PDO::PARAM_INT));
            /*
            $constraints [] = $query->in ( 'consultant_type', $consultantTypes );
            */
        }

        //$query->matching ( $query->logicalAnd ( $constraints ) );

        // does not work $query->setOrderings(array('competence_location_mm.sorting"' => \TYPO3\CMS\Extbase\Persistence\Generic\Query::ORDER_ASCENDING));

        #$GLOBALS['TYPO3_DB']->debugOutput = 2;


        $locations = $query->execute()->fetchAll();        /*
         * SELECT DISTINCT tx_bdlocations_domain_model_location.* FROM tx_bdlocations_domain_model_location LEFT JOIN tx_bdlocations_competence_location_mm ON tx_bdlocations_domain_model_location.uid=tx_bdlocations_competence_location_mm.uid_foreign LEFT JOIN tx_bdlocations_domain_model_competence ON tx_bdlocations_competence_location_mm.uid_local=tx_bdlocations_domain_model_competence.uid WHERE ((tx_bdlocations_domain_model_competence.division = '2' AND tx_bdlocations_domain_model_competence.iso_code IN ('DE')) AND tx_bdlocations_domain_model_location.consultant_type IN ('3','2','1','2','3','1')) AND tx_bdlocations_domain_model_location.deleted=0 AND tx_bdlocations_domain_model_location.t3ver_state<=0 AND tx_bdlocations_domain_model_location.pid<>-1 AND tx_bdlocations_domain_model_location.hidden=0 AND tx_bdlocations_domain_model_location.starttime<=1386079620 AND (tx_bdlocations_domain_model_location.endtime=0 OR tx_bdlocations_domain_model_location.endtime>1386079620) AND tx_bdlocations_domain_model_location.sys_language_uid IN (0,-1) AND tx_bdlocations_domain_model_location.pid IN (911) AND tx_bdlocations_domain_model_competence.deleted=0 AND tx_bdlocations_domain_model_competence.t3ver_state<=0 AND tx_bdlocations_domain_model_competence.pid<>-1 AND tx_bdlocations_domain_model_competence.hidden=0 AND tx_bdlocations_domain_model_competence.starttime<=1386079620 AND (tx_bdlocations_domain_model_competence.endtime=0 OR tx_bdlocations_domain_model_competence.endtime>1386079620) AND tx_bdlocations_domain_model_competence.sys_language_uid IN (0,-1) AND tx_bdlocations_domain_model_competence.pid IN (911)
         */

        // man is this ugly - extbase can not sort on mm table
        if ($locations) {

            $uidList = array ();
            foreach ( $locations as $location ) {
                $uidList [] = $location->getUid ();
            }
            $uidList = implode ( ',', $uidList );

            /* @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_location');

            $query = $queryBuilder->select()->from('tx_bmlocations_domain_model_location')->leftJoin(
                'tx_bmlocations_domain_model_location',
                'tx_bmlocations_competence_location_mm',
                'location_mm',
                 $query->expr()->eq('uid','location_mm.uid_foreign')
            )->leftJoin(
                'tx_bmlocations_domain_model_consultanttype',
                'tx_bmlocations_domain_model_location',
                'tx_bmlocations_domain_model_location.consultanttype=tx_bmlocations_domain_model_consultanttype.uid'
            )->where(
                $queryBuilder->expr()->in('uid', $uidList)
            )->orderBy('tx_bmlocations_domain_model_consultanttype.sorting', 'tx_bmlocations_competence_location_mm.sorting');

            /*
            $query->statement ( "
					SELECT DISTINCT tx_bdlocations_domain_model_location.*
					FROM tx_bdlocations_domain_model_location
					LEFT JOIN tx_bdlocations_competence_location_mm
					ON tx_bdlocations_domain_model_location.uid=tx_bdlocations_competence_location_mm.uid_foreign
					LEFT JOIN tx_bdlocations_domain_model_consultant_type
					ON tx_bdlocations_domain_model_location.consultant_type=tx_bdlocations_domain_model_consultant_type.uid
					WHERE tx_bdlocations_domain_model_location.uid IN ({$uidList})
					ORDER BY tx_bdlocations_domain_model_consultant_type.sorting,
						tx_bdlocations_competence_location_mm.sorting" );
            */
            $locations = $query->execute()->fetchAll();
        }

        return $locations;
    }

    /**
     * @param $isoCode
     * @param $division
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCountryAndDivision($isoCode, $division)
    {
        $division = intval($division);

        $query = $this->createQuery ();
        $query->statement ( "
			SELECT tx_bdlocations_domain_model_location.*
			FROM tx_bdlocations_domain_model_location
			LEFT JOIN tx_bdlocations_competence_location_mm
				ON (tx_bdlocations_competence_location_mm.uid_foreign = tx_bdlocations_domain_model_location.uid)
			LEFT JOIN tx_bdlocations_domain_model_competence
				ON (tx_bdlocations_competence_location_mm.uid_local = tx_bdlocations_domain_model_competence.uid)
			WHERE ($division=0 OR tx_bdlocations_domain_model_competence.division = ?)
			AND tx_bdlocations_domain_model_competence.iso_code = ?
			AND tx_bdlocations_domain_model_location.deleted=0
			AND tx_bdlocations_domain_model_location.hidden=0
			AND tx_bdlocations_domain_model_competence.deleted=0
			AND tx_bdlocations_domain_model_competence.hidden=0
			ORDER BY tx_bdlocations_competence_location_mm.sorting", array (
            $division,
            $isoCode,
        ) );
        $locations = $query->execute ();
        return $locations;
    }

    /**
     * @param $region
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByRegion($region)
    {
        $query = $this->createQuery ();
        $query->statement ( "
			SELECT tx_bdlocations_domain_model_location.*
				FROM tx_bdlocations_domain_model_location
				LEFT JOIN tx_bdlocations_location_region_mm
					ON (tx_bdlocations_location_region_mm.uid_local = tx_bdlocations_domain_model_location.uid)
				WHERE tx_bdlocations_location_region_mm.uid_foreign = ?
								AND tx_bdlocations_domain_model_location.deleted=0
								AND tx_bdlocations_domain_model_location.hidden=0
				ORDER BY tx_bdlocations_location_region_mm.sorting", array (
            $region->getUid (),
        ) );

        $locations = $query->execute ();
        return $locations;
    }

    /**
     * @param $region
     * @param $consultantType
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByRegionAndConsultantType($region, $consultantType)
    {
        if ($consultantType and ($consultantTypes = explode ( ',', $consultantType ))) {
            $uidList = array ();
            foreach ( $consultantTypes as $type ) {
                $uidList [] = intval ( $type );
            }
            $uidList = implode ( ',', $uidList );
        }

        $query = $this->createQuery ();
        $query->statement ( "
				SELECT tx_bdlocations_domain_model_location.*
				FROM tx_bdlocations_domain_model_location
				LEFT JOIN tx_bdlocations_location_region_mm
				ON (tx_bdlocations_location_region_mm.uid_local = tx_bdlocations_domain_model_location.uid)
				LEFT JOIN tx_bdlocations_competence_location_mm
				ON tx_bdlocations_domain_model_location.uid=tx_bdlocations_competence_location_mm.uid_foreign
				WHERE tx_bdlocations_location_region_mm.uid_foreign = ?
				AND tx_bdlocations_domain_model_location.consultant_type IN ({$uidList})
				AND tx_bdlocations_domain_model_location.deleted=0
				AND tx_bdlocations_domain_model_location.hidden=0
				ORDER BY tx_bdlocations_competence_location_mm.sorting", array (
            $region->getUid (),
        ) );

        $locations = $query->execute ();

        return $locations;
    }

    /**
     * @param $isoCode
     * @param $division
     * @param $consultantType
     * @param $coordinates
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByDistance($isoCode, $division, $consultantType, $coordinates)
    {
        if ($consultantType and ($consultantTypes = explode ( ',', $consultantType ))) {
            $uidList = array ();
            foreach ( $consultantTypes as $type ) {
                $uidList [] = intval ( $type );
            }
            $uidList = implode ( ',', $uidList );
        }

        list ( $Latitude, $Longitude ) = explode ( ',', $coordinates );

        // $strHavingClause .= 'distance<=100'; // km

        $division = intval($division);

        $query = $this->createQuery ();
        $query->statement ( "
				SELECT tx_bdlocations_domain_model_location.*,
				(6371 * acos(cos(radians(" . ( float ) $Latitude . "))
				* cos(radians(tx_bdlocations_domain_model_location.latitude))
					* cos(radians(tx_bdlocations_domain_model_location.longitude) - radians(" . ( float ) $Longitude . ")) + sin(radians(" . ( float ) $Latitude . "))
					* sin(radians(tx_bdlocations_domain_model_location.latitude)))) AS distance
				FROM tx_bdlocations_domain_model_location
				LEFT JOIN tx_bdlocations_competence_location_mm
				ON (tx_bdlocations_competence_location_mm.uid_foreign = tx_bdlocations_domain_model_location.uid)
				LEFT JOIN tx_bdlocations_domain_model_competence
				ON (tx_bdlocations_competence_location_mm.uid_local = tx_bdlocations_domain_model_competence.uid)
				LEFT JOIN tx_bdlocations_zip_range
				ON (tx_bdlocations_zip_range.parent_id = tx_bdlocations_domain_model_location.uid)
				WHERE ($division=0 OR tx_bdlocations_domain_model_competence.division = ?)
				AND tx_bdlocations_domain_model_competence.iso_code = ?
				AND tx_bdlocations_domain_model_location.consultant_type IN ({$uidList})
				AND tx_bdlocations_domain_model_location.deleted=0
				AND tx_bdlocations_domain_model_location.hidden=0
				AND tx_bdlocations_domain_model_competence.deleted=0
				AND tx_bdlocations_domain_model_competence.hidden=0
				ORDER BY distance LIMIT 5", array (
            $division,
            $isoCode,
        ) );
        $locations = $query->execute ();
        return $locations;
    }

    /**
     * findByCountryAndDivisionAndZip
     *
     * @param string $isoCode
     * @param string $division
     * @param string $zip
     * @return array
     *
     */
    public function findByCountryAndDivisionAndZip($isoCode, $division, $zip)
    {

        $query = $this->createQuery();
        $query->matching($query->logicalAnd(array(
            $query->equals('competence.division', $division),
            $query->equals('competence.isoCode', $isoCode),
            $query->logicalOr(array(
                $query->equals('type', '1'),
                $query->logicalAnd(array(
                    $query->logicalNot($query->equals('zip', '')),
                    $query->logicalNot($query->equals('zipRanges', '')),
                )),
            )),
        )));

        $query->setOrderings(array(
            'consultantType.sorting' => QueryInterface::ORDER_ASCENDING
        ));

        $results = $query->execute();

        $matchingLocations = array();

        if ($results) {
            /** @var Location $location */
            foreach ($results as $location) {
                $zipRange = $location->getZipRanges();
                $zipRanges = explode(PHP_EOL, $zipRange);
                foreach ($zipRanges as $range) {
                    list($lower, $upper) = explode('-', $range);
                    $lower = intval($lower);
                    $upper = intval($upper);
                    $zip = intval($zip);
                    if ($zip >= $lower && $zip <= $upper) {
                        $matchingLocations[] = $location;
                    }
                }
            }
        }

        return $matchingLocations;
    }

    /**
     * @param $competence
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCompetence($competence)
    {
        $query = $this->createQuery ();
        $query->statement ( "
			SELECT tx_bdlocations_domain_model_location.*
			FROM tx_bdlocations_domain_model_location
			LEFT JOIN tx_bdlocations_competence_location_mm
			ON (tx_bdlocations_competence_location_mm.uid_foreign = tx_bdlocations_domain_model_location.uid)
			LEFT JOIN tx_bdlocations_domain_model_competence
				ON (tx_bdlocations_competence_location_mm.uid_local = tx_bdlocations_domain_model_competence.uid)
			WHERE tx_bdlocations_domain_model_competence.uid = ?
			AND tx_bdlocations_domain_model_location.deleted=0
			AND tx_bdlocations_domain_model_location.hidden=0
			AND tx_bdlocations_domain_model_competence.deleted=0
			AND tx_bdlocations_domain_model_competence.hidden=0
			ORDER BY tx_bdlocations_competence_location_mm.sorting", array (
            $competence->getUid (),
        ) );
        $locations = $query->execute ();
        return $locations;
    }
}
