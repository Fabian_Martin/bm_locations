<?php
namespace Bitmotion\BmLocations\Domain\Repository;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * The repository for Regions
 */
class RegionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * Returns a query for objects of this repository
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     * @api
     */
    public function createQuery() {
        $query = parent:: createQuery();
        $query->getQuerySettings()->setLanguageMode('content_fallback');
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        return $query;
    }


    public function findOneByMapRegionIdAndDivision($regionId, $division)
    {
        $query = $this->createQuery();

        $constraints = array();
        if ($division) {
            $constraints[] = $query->equals('country.division', $division);
        }
        $constraints[] = $query->equals('map_region_id', $regionId);

        $query->matching(
            $query->logicalAnd(
                $constraints
            )
        );
        $query->setLimit(1);

        $regions = $query->execute();
        return reset($regions->toArray());
    }


    public function findOneByCountryAndDivision($isoCode, $division)
    {
        /* @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_bmlocations_domain_model_region');
        $query = $queryBuilder->select()->from('tx_bmlocations_domain_model_region')->where(
            $queryBuilder->expr()->eq('country.division', $division)
        )->andWhere(
            $queryBuilder->expr()->eq('country.uid', $isoCode)
        );
/*
        $query = $this->createQuery();

        $constraints = array();

        $constraints[] = $query->equals('country.division', $division);
        $constraints[] = $query->equals('country.iso_code', $isoCode);

        $query->matching(
            $query->logicalAnd(
                $constraints
            )
        );
        $query->setLimit(1);

        $regions = $query->execute();
*/
        return reset($query->execute()->fetchAll());
    }
}
