<?php
namespace Bitmotion\BmLocations\Domain\Model;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Competence
 */
class Competence extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * country
     *
     * @var \SJBR\StaticInfoTables\Domain\Model\Country
     */
    protected $country = null;

    /**
     * division
     *
     * @var \Bitmotion\BmLocations\Domain\Model\Division
     */
    protected $division = null;

    /**
     * location
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location>
     */
    protected $location = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->location = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the country
     *
     * @return \SJBR\StaticInfoTables\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\SJBR\StaticInfoTables\Domain\Model\Country $country)
    {
        $this->country = $country;
    }

    /**
     * Returns the division
     *
     * @return \Bitmotion\BmLocations\Domain\Model\Division $division
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * Sets the division
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Division $division
     * @return void
     */
    public function setDivision(\Bitmotion\BmLocations\Domain\Model\Division $division)
    {
        $this->division = $division;
    }

    /**
     * Adds a Location
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Location $location
     * @return void
     */
    public function addLocation(\Bitmotion\BmLocations\Domain\Model\Location $location)
    {
        $this->location->attach($location);
    }

    /**
     * Removes a Location
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Location $locationToRemove The Location to be removed
     * @return void
     */
    public function removeLocation(\Bitmotion\BmLocations\Domain\Model\Location $locationToRemove)
    {
        $this->location->detach($locationToRemove);
    }

    /**
     * Returns the location
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location> $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location> $location
     * @return void
     */
    public function setLocation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $location)
    {
        $this->location = $location;
    }
}
