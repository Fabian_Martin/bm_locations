<?php
namespace Bitmotion\BmLocations\Domain\Model;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

use Bitmotion\BmLocations\Domain\Repository\CompetenceRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Location
 */
class Location extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * longitude
     *
     * @var string
     */
    protected $longitude = '';

    /**
     * latitude
     *
     * @var string
     */
    protected $latitude = '';

    /**
     * adress
     *
     * @var string
     */
    protected $adress = '';

    /**
     * zip
     *
     * @var int
     */
    protected $zip = 0;

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * autoCoordinates
     *
     * @var bool
     */
    protected $autoCoordinates = false;

    /**
     * hideLocation
     *
     * @var bool
     */
    protected $hideLocation = false;

    /**
     * hideAdress
     *
     * @var bool
     */
    protected $hideAdress = false;

    /**
     * zipRanges
     *
     * @var string
     */
    protected $zipRanges = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * noCountryListInPdf
     *
     * @var bool
     */
    protected $noCountryListInPdf = false;

    /**
     * segment
     *
     * @var string
     */
    protected $segment = '';

    /**
     * regionName
     *
     * @var string
     */
    protected $regionName = '';

    /**
     * competence
     *
     * @var \Bitmotion\BmLocations\Domain\Model\Competence
     */
    protected $competence = null;

    /**
     * region
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Region>
     */
    protected $region = null;

    /**
     * consultantType
     *
     * @var \Bitmotion\BmLocations\Domain\Model\ConsultantType
     */
    protected $consultantType = null;

    /**
     * country
     *
     * @var \SJBR\StaticInfoTables\Domain\Model\Country
     */
    protected $country = null;

    /**
     * contact
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Contact>
     * @cascade remove
     */
    protected $contact = null;

    /**
     * Use address from other location
     *
     * @var int
     * @var \Bitmotion\BmLocations\Domain\Model\Location
     * @lazy
     */
    protected $baseAddress;

    /**
     * Consultant
     *
     * @var int
     * @var \Bitmotion\BmLocations\Domain\Model\Consultant
     *
     */
    protected $consultant;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->region = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->contact = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the longitude
     *
     * @return string $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude
     *
     * @param string $longitude
     * @return void
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * Returns the latitude
     *
     * @return string $latitude
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude
     *
     * @param string $latitude
     * @return void
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Returns the adress
     *
     * @return string $adress
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Sets the adress
     *
     * @param string $adress
     * @return void
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * Returns the zip
     *
     * @return int $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param int $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the autoCoordinates
     *
     * @return bool $autoCoordinates
     */
    public function getAutoCoordinates()
    {
        return $this->autoCoordinates;
    }

    /**
     * Sets the autoCoordinates
     *
     * @param bool $autoCoordinates
     * @return void
     */
    public function setAutoCoordinates($autoCoordinates)
    {
        $this->autoCoordinates = $autoCoordinates;
    }

    /**
     * Returns the boolean state of autoCoordinates
     *
     * @return bool
     */
    public function isAutoCoordinates()
    {
        return $this->autoCoordinates;
    }

    /**
     * Returns the hideLocation
     *
     * @return bool $hideLocation
     */
    public function getHideLocation()
    {
        return $this->hideLocation;
    }

    /**
     * Sets the hideLocation
     *
     * @param bool $hideLocation
     * @return void
     */
    public function setHideLocation($hideLocation)
    {
        $this->hideLocation = $hideLocation;
    }

    /**
     * Returns the boolean state of hideLocation
     *
     * @return bool
     */
    public function isHideLocation()
    {
        return $this->hideLocation;
    }

    /**
     * Returns the hideAdress
     *
     * @return bool $hideAdress
     */
    public function getHideAdress()
    {
        return $this->hideAdress;
    }

    /**
     * Sets the hideAdress
     *
     * @param bool $hideAdress
     * @return void
     */
    public function setHideAdress($hideAdress)
    {
        $this->hideAdress = $hideAdress;
    }

    /**
     * Returns the boolean state of hideAdress
     *
     * @return bool
     */
    public function isHideAdress()
    {
        return $this->hideAdress;
    }

    /**
     * Returns the zipRanges
     *
     * @return string $zipRanges
     */
    public function getZipRanges()
    {
        return $this->zipRanges;
    }

    /**
     * Sets the zipRanges
     *
     * @param string $zipRanges
     * @return void
     */
    public function setZipRanges($zipRanges)
    {
        $this->zipRanges = $zipRanges;
    }

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the noCountryListInPdf
     *
     * @return bool $noCountryListInPdf
     */
    public function getNoCountryListInPdf()
    {
        return $this->noCountryListInPdf;
    }

    /**
     * Sets the noCountryListInPdf
     *
     * @param bool $noCountryListInPdf
     * @return void
     */
    public function setNoCountryListInPdf($noCountryListInPdf)
    {
        $this->noCountryListInPdf = $noCountryListInPdf;
    }

    /**
     * Returns the boolean state of noCountryListInPdf
     *
     * @return bool
     */
    public function isNoCountryListInPdf()
    {
        return $this->noCountryListInPdf;
    }

    /**
     * Returns the segment
     *
     * @return string $segment
     */
    public function getSegment()
    {
        return $this->segment;
    }

    /**
     * Sets the segment
     *
     * @param string $segment
     * @return void
     */
    public function setSegment($segment)
    {
        $this->segment = $segment;
    }

    /**
     * Returns the regionName
     *
     * @return string $regionName
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * Sets the regionName
     *
     * @param string $regionName
     * @return void
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
    }

    /**
     * Returns the competence
     *
     * @return \Bitmotion\BmLocations\Domain\Model\Competence $competence
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * Sets the competence
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Competence $competence
     * @return void
     */
    public function setCompetence(\Bitmotion\BmLocations\Domain\Model\Competence $competence)
    {
        $this->competence = $competence;
    }

    /**
     * Adds a Region
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Region $region
     * @return void
     */
    public function addRegion(\Bitmotion\BmLocations\Domain\Model\Region $region)
    {
        $this->region->attach($region);
    }

    /**
     * Removes a Region
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Region $regionToRemove The Region to be removed
     * @return void
     */
    public function removeRegion(\Bitmotion\BmLocations\Domain\Model\Region $regionToRemove)
    {
        $this->region->detach($regionToRemove);
    }

    /**
     * Returns the region
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Region> $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Sets the region
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Region> $region
     * @return void
     */
    public function setRegion(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $region)
    {
        $this->region = $region;
    }

    /**
     * Returns the consultantType
     *
     * @return \Bitmotion\BmLocations\Domain\Model\ConsultantType $consultantType
     */
    public function getConsultantType()
    {
        return $this->consultantType;
    }

    /**
     * Sets the consultantType
     *
     * @param \Bitmotion\BmLocations\Domain\Model\ConsultantType $consultantType
     * @return void
     */
    public function setConsultantType(\Bitmotion\BmLocations\Domain\Model\ConsultantType $consultantType)
    {
        $this->consultantType = $consultantType;
    }

    /**
     * Returns the country
     *
     * @return \SJBR\StaticInfoTables\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param \SJBR\StaticInfoTables\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\SJBR\StaticInfoTables\Domain\Model\Country $country)
    {
        $this->country = $country;
    }

    /**
     * Adds a Contact
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Contact $contact
     * @return void
     */
    public function addContact(\Bitmotion\BmLocations\Domain\Model\Contact $contact)
    {
        $this->contact->attach($contact);
    }

    /**
     * Removes a Contact
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Contact $contactToRemove The Contact to be removed
     * @return void
     */
    public function removeContact(\Bitmotion\BmLocations\Domain\Model\Contact $contactToRemove)
    {
        $this->contact->detach($contactToRemove);
    }

    /**
     * Returns the contact
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Contact> $contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Sets the contact
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Contact> $contact
     * @return void
     */
    public function setContact(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Returns the baseAddress
     *
     * @return \Bitmotion\BmLocations\Domain\Model\Location $baseAddress
     */
    public function getBaseAddress() {
        if ($this->type==0) {
            return $this;
        }

        if (!is_object($this->baseAddress)) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Tx_Extbase_Object_ObjectManager');

            /** @var $locationRepository \Bitmotion\BmLocations\Domain\Repository\LocationRepository */
            $locationRepository = $objectManager->get('Bitmotion\\BmLocations\\Domain\\Repository\\LocationRepository');
            $this->baseAddress = $locationRepository->findByUid($this->baseAddress);
        }

        return $this->baseAddress;
    }

    /**
     * Returns the consultant
     *
     * @return \Bitmotion\BmLocations\Domain\Model\Consultant $consultant
     */
    public function getConsultant() {

        if (!is_object($this->consultant)) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
            /** @var $consultantRepository \Bitmotion\BmLocations\Domain\Repository\ConsultantRepository */
            $consultantRepository = $objectManager->get('Bitmotion\\BmLocations\\Domain\\Repository\\ConsultantRepository');
            $this->consultant = $consultantRepository->findByUid($this->consultant);
        }

        return $this->consultant;
    }

    /**
     * @return mixed
     */
    public function getLocationForRouting() {
        if ($this->autoCoordinates) {
            $strToReturn = $this->adress . ',' . $this->city . ',' . $this->zip;
        } else {
            $strToReturn = $this->latitude . ',' . $this->longitude;
        }

        return str_replace(array("\r\n", "\r"), ",", $strToReturn);
    }

    /**
     * Returns the country
     *
     * @return string
     */
    public function getCountryName() {
        if (!$this->country) {
            return null;
        }
        return \Bitmotion\BmLocations\Utility\StaticInfoTablesHelper::getTitleFromIsoCode ('static_countries', $this->country);
    }
}
