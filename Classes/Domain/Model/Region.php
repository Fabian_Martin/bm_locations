<?php
namespace Bitmotion\BmLocations\Domain\Model;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Region
 */
class Region extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * mapRegionId
     *
     * @var string
     */
    protected $mapRegionId = '';

    /**
     * competence
     *
     * @var \Bitmotion\BmLocations\Domain\Model\Competence
     */
    protected $competence = null;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the mapRegionId
     *
     * @return string $mapRegionId
     */
    public function getMapRegionId()
    {
        return $this->mapRegionId;
    }

    /**
     * Sets the mapRegionId
     *
     * @param string $mapRegionId
     * @return void
     */
    public function setMapRegionId($mapRegionId)
    {
        $this->mapRegionId = $mapRegionId;
    }

    /**
     * Returns the competence
     *
     * @return \Bitmotion\BmLocations\Domain\Model\Competence $competence
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * Sets the competence
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Competence $competence
     * @return void
     */
    public function setCompetence(\Bitmotion\BmLocations\Domain\Model\Competence $competence)
    {
        $this->competence = $competence;
    }
}
