<?php
namespace Bitmotion\BmLocations\Domain\Model;

/***
 *
 * This file is part of the "bm_locations" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Consultant
 */
class Consultant extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * noCountryListInPdf
     *
     * @var bool
     */
    protected $noCountryListInPdf = false;

    /**
     * locations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location>
     * @cascade remove
     */
    protected $locations = null;

    /**
     * socialMedia
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmSocialMedia\Domain\Model\Profile>
     * @cascade remove
     */
    protected $socialMedia = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->locations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->socialMedia = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the noCountryListInPdf
     *
     * @return bool $noCountryListInPdf
     */
    public function getNoCountryListInPdf()
    {
        return $this->noCountryListInPdf;
    }

    /**
     * Sets the noCountryListInPdf
     *
     * @param bool $noCountryListInPdf
     * @return void
     */
    public function setNoCountryListInPdf($noCountryListInPdf)
    {
        $this->noCountryListInPdf = $noCountryListInPdf;
    }

    /**
     * Returns the boolean state of noCountryListInPdf
     *
     * @return bool
     */
    public function isNoCountryListInPdf()
    {
        return $this->noCountryListInPdf;
    }

    /**
     * Adds a Location
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Location $location
     * @return void
     */
    public function addLocation(\Bitmotion\BmLocations\Domain\Model\Location $location)
    {
        $this->locations->attach($location);
    }

    /**
     * Removes a Location
     *
     * @param \Bitmotion\BmLocations\Domain\Model\Location $locationToRemove The Location to be removed
     * @return void
     */
    public function removeLocation(\Bitmotion\BmLocations\Domain\Model\Location $locationToRemove)
    {
        $this->locations->detach($locationToRemove);
    }

    /**
     * Returns the locations
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location> $locations
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Sets the locations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmLocations\Domain\Model\Location> $locations
     * @return void
     */
    public function setLocations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $locations)
    {
        $this->locations = $locations;
    }

    /**
     * Adds a Profile
     *
     * @param \Bitmotion\BmSocialMedia\Domain\Model\Profile $socialMedium
     * @return void
     */
    public function addSocialMedium(\Bitmotion\BmSocialMedia\Domain\Model\Profile $socialMedium)
    {
        $this->socialMedia->attach($socialMedium);
    }

    /**
     * Removes a Profile
     *
     * @param \Bitmotion\BmSocialMedia\Domain\Model\Profile $socialMediumToRemove The Profile to be removed
     * @return void
     */
    public function removeSocialMedium(\Bitmotion\BmSocialMedia\Domain\Model\Profile $socialMediumToRemove)
    {
        $this->socialMedia->detach($socialMediumToRemove);
    }

    /**
     * Returns the socialMedia
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmSocialMedia\Domain\Model\Profile> $socialMedia
     */
    public function getSocialMedia()
    {
        return $this->socialMedia;
    }

    /**
     * Sets the socialMedia
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Bitmotion\BmSocialMedia\Domain\Model\Profile> $socialMedia
     * @return void
     */
    public function setSocialMedia(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $socialMedia)
    {
        $this->socialMedia = $socialMedia;
    }
}
